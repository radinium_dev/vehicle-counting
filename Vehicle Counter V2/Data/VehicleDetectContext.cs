﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicle_Counter_V2.Data
{
    class VehicleDetectContext : DbContext
    {
        public VehicleDetectContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {
        }

        public VehicleDetectContext()
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<VehicleDetectContext, Migrations.Configuration>());
        }

        public DbSet<VehicleCount> VehicleCounts { get; set; }
        public DbSet<VehicleCountSummary> VehicleCountSummary { get; set; }
    }
}
