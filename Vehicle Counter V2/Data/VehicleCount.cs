﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicle_Counter_V2.Data
{
    class VehicleCount
    {
        [Key]
        public int ID { set; get; }

        [Index]
        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string CameraItemID { set; get; }

        [Index]
        public DateTime CountedAt { set; get; }

        public bool Inbound { set; get; }

        [Index]
        [Column(TypeName = "VARCHAR")]
        [StringLength(40)]
        public string ObjectName { set; get; }
    }
}
