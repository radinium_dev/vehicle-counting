IF (NOT EXISTS (SELECT name 
FROM master.dbo.sysdatabases 
WHERE ('[' + name + ']' = 'Radinium'
OR name = 'Radinium')))
BEGIN
	CREATE DATABASE Radinium;
END
GO

USE Radinium
GO
IF NOT EXISTS(SELECT principal_id FROM sys.server_principals WHERE name = 'RadiniumClassifier')
	CREATE LOGIN RadiniumClassifier WITH PASSWORD='radinium@01!', CHECK_POLICY=OFF, CHECK_EXPIRATION=OFF;
GO
IF NOT EXISTS(SELECT principal_id FROM sys.database_principals WHERE name = 'RadiniumClassifier')
	CREATE USER RadiniumClassifier FOR LOGIN RadiniumClassifier ;
GO
ALTER ROLE db_owner ADD MEMBER RadiniumClassifier;
GO
GRANT ALL TO RadiniumClassifier;
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'RadiniumClassifier')
	EXEC('CREATE SCHEMA RadiniumClassifier AUTHORIZATION RadiniumClassifier');
GO
ALTER USER RadiniumClassifier WITH DEFAULT_SCHEMA=RadiniumClassifier;
GO

USE master
GO
EXEC xp_instance_regwrite N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'LoginMode', REG_DWORD, 2
GO