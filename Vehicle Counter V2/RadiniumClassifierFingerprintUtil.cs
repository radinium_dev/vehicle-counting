﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cygnetic.MIPPluginCommons.Licence;
using Cygnetic.MIPPluginCommons.Util;
using Newtonsoft.Json;
using Vehicle_Counter_V2;
using VideoOS.Platform;
using VideoOS.Platform.ConfigurationItems;
using VideoOS.Platform.Messaging;

namespace Vehicle_Counter_V2
{
    class RadiniumClassifierFingerprintUtil : FingerprintUtil
    {
        public override string GenerateFingerprint(object[] parts)
        {
            if (parts != null && parts.Count() == 1)
            {
                return parts[0].ToString();
            }
            string fullIdentifier = CpuId() + ":" + BiosId() + ":" + MotherboardId();
            return Base64(Base64(Vehicle_Counter_V2Definition.LicenceRequestType) + ":" + Base64(fullIdentifier));

        }

    }
}
