namespace Vehicle_Counter_V2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VehicleCountSummaries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CameraItemID = c.String(maxLength: 40, unicode: false),
                        Inbound = c.Boolean(nullable: false),
                        ObjectName = c.String(maxLength: 40, unicode: false),
                        SecondsTimespan = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.CameraItemID)
                .Index(t => t.ObjectName)
                .Index(t => t.StartTime);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.VehicleCountSummaries", new[] { "StartTime" });
            DropIndex("dbo.VehicleCountSummaries", new[] { "ObjectName" });
            DropIndex("dbo.VehicleCountSummaries", new[] { "CameraItemID" });
            DropTable("dbo.VehicleCountSummaries");
        }
    }
}
