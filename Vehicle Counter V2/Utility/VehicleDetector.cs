﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace Vehicle_Counter_V2.Utility
{
    public class VehicleDetector
    {
        public Item Item;

        public VehicleDetector(Item item)
        {
            Item = item;
        }

        public string Name
        {
            get { return Item.Name; }
            set { Item.Name = value; }
        }

        public bool IsNorthBoundIn
        {
            get { return Item.Properties.ContainsKey("is_northbound_in") ? bool.Parse(Item.Properties["is_northbound_in"]) : false; }
            set
            {
                if (Item.Properties.ContainsKey("is_northbound_in"))
                {
                    Item.Properties["is_northbound_in"] = value.ToString();
                }
                else
                {
                    Item.Properties.Add("is_northbound_in", value.ToString());
                }
            }
        }
        public virtual FQID CameraItemFQID { get; set; }        

        public Item CameraItem
        {
            get { return Item.Properties.ContainsKey("camera_item_fqid") ? Configuration.Instance.GetItem(new FQID(Item.Properties["camera_item_fqid"])) : null; }
            set
            {
                if (Item.Properties.ContainsKey("camera_item_fqid"))
                {
                    Item.Properties["camera_item_fqid"] = value.FQID.ToXmlNode().OuterXml;
                }
                else
                {
                    Item.Properties.Add("camera_item_fqid", value.FQID.ToXmlNode().OuterXml);
                }
            }
        }


        public String Group
        {
            get { return Item.Properties.ContainsKey("camera_group") ? Item.Properties["camera_group"] : null; }
            set
            {
                if (Item.Properties.ContainsKey("camera_group"))
                {
                    Item.Properties["camera_group"] = value;
                }
                else
                {
                    Item.Properties.Add("camera_group", value);
                }
            }
        }

        public String Config
        {
            get { return Item.Properties.ContainsKey("camera_config") ? Item.Properties["camera_config"] : null; }
            set
            {
                if (Item.Properties.ContainsKey("camera_config"))
                {
                    Item.Properties["camera_config"] = value;
                }
                else
                {
                    Item.Properties.Add("camera_config", value);
                }
            }
        }

        public virtual Dictionary<string, string> Properties { get; }
        public String Username
        {
            get { return Item.Properties.ContainsKey("camera_username") ? Item.Properties["camera_username"] : null; }
            set
            {
                if (Item.Properties.ContainsKey("camera_username"))
                {
                    Item.Properties["camera_username"] = value;
                }
                else
                {
                    Item.Properties.Add("camera_username", value);
                }
            }
        }

        public String Password
        {
            get { return Item.Properties.ContainsKey("camera_password") ? Item.Properties["camera_password"] : null; }
            set
            {
                if (Item.Properties.ContainsKey("camera_password"))
                {
                    Item.Properties["camera_password"] = value;
                }
                else
                {
                    Item.Properties.Add("camera_password", value);
                }
            }
        }        
        public override string ToString()
        {
            return String.IsNullOrEmpty(Name) ? "Unnamed" : Name;
        }



    }
}
