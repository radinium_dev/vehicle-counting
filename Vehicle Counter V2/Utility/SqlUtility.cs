﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using VideoOS.Platform;

namespace Vehicle_Counter_V2.Utility
{
    public class SqlUtility
    {
        public static SqlConnection GetSqlConnection()
        {
            XmlNode optionsNode = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            string username = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_USERNAME, optionsNode);
            string databaseHost = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_HOST, optionsNode);
            string password = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_PASSWORD, optionsNode);
            string databaseName = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_NAME, optionsNode);
            //if (databaseHost == "")
            {
                databaseName = "CarCounter";
                databaseHost = "10.4.24.30";
                username = "sa";
                password = "neoL9hlt3HdC";
            }
/*            if (databaseHost == "")
            {
                databaseName = "Vehicle_Counter_V2.Data.VehicleDetectContext";
                databaseHost = "192.168.0.43";
                username = "test";
                password = "test";
            }*/
            if (username != "")
            {
                builder.UserID = username;
                builder.Password = password;
            }
            else
            {
                builder.IntegratedSecurity = true;
            }

            builder.ConnectTimeout = 5;
            builder.DataSource = databaseHost;
            builder.InitialCatalog = databaseName;

            //string connStr = "Server=tcp:10.4.24.30,1433;Database=CarCounter;Uid=sa;Pwd=neoL9hlt3HdC;";
            //string connStr = String.Format(USERNAME_PASSWORD_SQL_CONNECTION_STRING, new object[] { username, password, databaseHost, databaseName});
            //string connStr = String.Format(CONNECT_STRING, new object[] { databaseHost, databaseName, username, password });
            string connStr = builder.ConnectionString;
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connStr;
            connection.Open();

            return connection;
        }
        internal static readonly string CONNECT_STRING = @"Server={0};Database={1};uid={2};pwd={3}";
        internal static readonly string USERNAME_PASSWORD_SQL_CONNECTION_STRING = @"UID={0};PWD={1};Server={2};Database={3};Connect Timeout=600";
    }
}
