using System;
using System.Linq;
using System.Windows.Forms;
using Vehicle_Counter_V2.Utility;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.UI;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Client;

namespace Vehicle_Counter_V2.Admin
{
    public partial class Vehicle_Counter_V2UserControl : UserControl
    {
        #region Fields
        internal event EventHandler ConfigurationChangedByUser;
        private Item _selectedCameraItem;

        private bool _rectMode;
        private bool isMouseDown = false;
        Rectangle rect;
        Rectangle selectedRect = new Rectangle();
        Point startXY;
        Point endXY;
        List<Rectangle> rectangles = new List<Rectangle>();

        List<AOI> aoiList = new List<AOI>();
        CamConfig camConfig = new CamConfig();
        private Rectangle tempRect;
        private int dragHandle = 0;
        private Point dragPoint;
        private BitmapSource _bitmapSource;
        private Item _myItem = null;
        #endregion

        #region Initialization
        public Vehicle_Counter_V2UserControl()
        {
            InitializeComponent();
            panelAOIEdit.Hide();

            panelVideoPreview.Hide();

            _bitmapSource = new BitmapSource();
            _bitmapSource.NewBitmapEvent += _bitmapSource_NewBitmapEvent;
            _bitmapSource.Selected = true;
        }

        void _bitmapSource_NewBitmapEvent(Bitmap bitmap)
        {
            if (pictureBox.Width != 0 && pictureBox.Height != 0)
            {
                Bitmap temp = new Bitmap(bitmap, pictureBox.Width, pictureBox.Height);

                pictureBox.Image = temp;
            }
            bitmap.Dispose();
        }
        #endregion
        #region ItemManager support methods

        internal void Close()
        {
            if (_bitmapSource != null)
            {
                CloseBitmap();
                _bitmapSource.NewBitmapEvent -= _bitmapSource_NewBitmapEvent;
                _bitmapSource = null;
            }
        }

        internal String DisplayName
        {
            get { return textBoxName.Text; }
            set { textBoxName.Text = value; }
        }

        internal String Username
        {
            get { return textBoxUsername.Text; }
            set { textBoxUsername.Text = value; }
        }

        internal String Password
        {
            get { return textBoxPassword.Text; }
            set { textBoxPassword.Text = value; }
        }

        private bool _ignoreChanges = false;
        internal void OnUserChange(object sender, EventArgs e)
        {
            if (!_ignoreChanges && ConfigurationChangedByUser != null)
                ConfigurationChangedByUser(this, new EventArgs());
        }

        internal Item CameraItem
        {
            get { return _selectedCameraItem; }
            set
            {
                _selectedCameraItem = value;
                buttonCameraSelect.Text = value == null ? "Select Camera" : value.Name;
            }            
        }        

        internal string Group
        {
            get { return textBoxGroup.Text; }
            set { textBoxGroup.Text = value; }
        }

        private bool bitmapInited = false;

        private void InitBitmap()
        {
            if (_bitmapSource != null && !bitmapInited)
            {
                bitmapInited = true;
                _bitmapSource.Init();
            }
        }

        private void CloseBitmap()
        {
            if (_bitmapSource != null && bitmapInited)
            {
                bitmapInited = false;
                _bitmapSource.Close();
            }
        }

        internal void FillContent(VehicleDetector vehicleDetector)
        {
            _myItem = vehicleDetector.Item;
            _ignoreChanges = true;
            ClearContent();

            if (vehicleDetector == null)
                return;
            textBoxName.Text = vehicleDetector.Name;
            CameraItem = vehicleDetector.CameraItem;
            Group = vehicleDetector.Group;
            DisplayName = vehicleDetector.Name;
            Username = vehicleDetector.Username;
            Password = vehicleDetector.Password;

            if (vehicleDetector.Item.Properties.ContainsKey("camera_item_fqid"))        // Get the FQID of previous selected camera
            {
                _selectedCameraItem = Configuration.Instance.GetItem(new FQID(vehicleDetector.Item.Properties["camera_item_fqid"]));
                buttonCameraSelect.Text = "";
                if (_selectedCameraItem == null)
                {
                    // Device deleted 
                    vehicleDetector.Item.Properties.Remove("camera_item_fqid");
                    vehicleDetector.Name += " (Camera deleted)";
                    return;
                }
                else
                {
                    buttonCameraSelect.Text = _selectedCameraItem.Name;                    
                    _bitmapSource.Item = _selectedCameraItem;
                    InitBitmap();
                    panelVideoPreview.Show();
                    _bitmapSource.LiveStart();
                }

                if (vehicleDetector.Item.Properties.ContainsKey("camera_config"))
                {
                    camConfig = JsonConvert.DeserializeObject<CamConfig>(vehicleDetector.Item.Properties["camera_config"]);
                    if (camConfig.serverIP != null)                    
                        textBoxServerIP.Text = camConfig.serverIP;   
                    if (camConfig.areasOfInterest != null)
                    {    
                        foreach (AOI aoi in camConfig.areasOfInterest)
                            rectangles.Add(aoi.rect);
                        pictureBox.Refresh();
                    }
                    checkBoxInboundUp.Checked = camConfig.inboundUp;
                }     
            }
            _ignoreChanges = false;
        }

        internal void ClearContent()
        {
            textBoxName.Text = "";
            buttonCameraSelect.Text = "Select camera...";
            if (_bitmapSource != null)
            {
                _bitmapSource.LiveStop();
                CloseBitmap();
            }
            ResetUserControl();
        }

        internal void ResetUserControl()
        {
            textBoxUsername.Text = "";
            textBoxPassword.Text = "";
            DisplayName = "";
            textBoxName.Text = "";
            textBoxGroup.Text = "";
            textBoxServerIP.Text = "";
            pictureBox.Image = null;
            checkBoxInboundUp.Checked = false;
            checkBoxInbound.Checked = false;
            checkBoxOutbound.Checked = false;
            panelVideoPreview.Hide();
        }

        internal void UpdateItem(VehicleDetector vehicleDetector)
        {
            vehicleDetector.Name = DisplayName;
            if (_selectedCameraItem != null)
            {
                vehicleDetector.Item.Properties["camera_item_fqid"] = _selectedCameraItem.FQID.ToXmlNode().OuterXml;
                
                vehicleDetector.Item.Properties["camera_config"] = JsonConvert.SerializeObject(camConfig);
            }
            vehicleDetector.CameraItem = CameraItem;
            vehicleDetector.Username = Username;
            vehicleDetector.Password = Password;
            vehicleDetector.Group = Group;


            if (String.IsNullOrEmpty(vehicleDetector.Name) && String.IsNullOrEmpty(textBoxName.Text)) //If unnamed, try use camera's name
            {
                if (vehicleDetector.CameraItem != null)
                {
                    vehicleDetector.Name = vehicleDetector.CameraItem.Name;
                }
                else
                {
                    vehicleDetector.Name = "Unnamed detector";
                }
            }
            else if (!String.IsNullOrEmpty(textBoxName.Text)) //Use input from textbox
            {
                vehicleDetector.Name = textBoxName.Text;
            }
            else
            {
                vehicleDetector.Name = "Unnamed detector";
            }

            Configuration.Instance.SaveItemConfiguration(Vehicle_Counter_V2Definition.Vehicle_Counter_V2PluginId, vehicleDetector.Item);
        }

        #endregion        
        #region Button Click Handling
                
        void buttonSaveConfig_Click(object sender, EventArgs e)
        {
            SaveConfig(_myItem);

            List<Item> allConfigList = Configuration.Instance.GetItemConfigurations(Vehicle_Counter_V2Definition.Vehicle_Counter_V2PluginId, null, Vehicle_Counter_V2Definition.Vehicle_Counter_V2Kind);
            List<CamConfig> tempList = new List<CamConfig>();

            foreach(Item conf in allConfigList)
            {
                CamConfig tempConf;
                if (conf.Properties.ContainsKey("camera_config"))
                {
                    tempConf = JsonConvert.DeserializeObject<CamConfig>(conf.Properties["camera_config"]);
                    if(tempConf.serverIP == textBoxServerIP.Text)
                    {
                        tempList.Add(tempConf);
                    }
                }
            }

            // Send captured rectangles to server in an acceptable format. Use TCP  
 
            string outObject = JsonConvert.SerializeObject(tempList);
            byte[] stringBuffer = UTF8Encoding.UTF8.GetBytes(outObject);
 
            try
            {
                // Establish the remote endpoint  
                // for the socket. This example  
                // uses port 11111 on the local  
                // computer. 
                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddr = ipHost.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
                if(ipAddr == null)
                {
                    return;
                }
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);

                // Creation TCP/IP Socket using  
                // Socket Class Costructor 
                Socket tcpSender = new Socket(ipAddr.AddressFamily,
                           SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    // Connect Socket to the remote  
                    // endpoint using method Connect() 
                    tcpSender.Connect(localEndPoint);
                    
                    tcpSender.Send(stringBuffer);
                    // Close Socket using  
                    // the method Close() 
                    tcpSender.Close();
                }

                // Manage of Socket's Exceptions 
                catch (ArgumentNullException ane)
                {

                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }

                catch (SocketException se)
                {

                    Console.WriteLine("SocketException : {0}", se.ToString());
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Unexpected exception : {0}", ex.ToString());
                }
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }
        }

        private void AOIDoneButton_Click(object sender, EventArgs e)
        {
            AOI tempAoi;
            bool tempFound = false;
            panelAOIEdit.Hide();
            if (aoiList.Count > 0)
            {
                foreach (AOI aoi in aoiList)
                {
                    if (tempRect == aoi.rect)
                    {
                        tempAoi = aoi;
                        tempFound = true;
                        tempAoi.rect = selectedRect;
                        if (checkBoxInbound.Checked && checkBoxOutbound.Checked)
                        {
                            tempAoi.oneDirection = "both-directions";
                        }
                        else if (checkBoxInbound.Checked)
                        {
                            tempAoi.oneDirection = "inbound";
                        }
                        else if (checkBoxOutbound.Checked)
                        {
                            tempAoi.oneDirection = "outbound";
                        }
                        else if (!checkBoxInbound.Checked && !checkBoxOutbound.Checked)
                        {
                            tempAoi.oneDirection = "both-directions";
                        }
                        aoiList[aoiList.IndexOf(aoi)] = tempAoi;
                        break;
                    }
                }
                if (!tempFound)
                    AddAOItoList();
            }
            else
                AddAOItoList();

            selectedRect = new Rectangle();
            pictureBox.Refresh();
        }

        private void AOIDeleteButton_Click(object sender, EventArgs e)
        {
            if (aoiList.Count > 0)
            {
                foreach (AOI aoi in aoiList)
                {
                    if (tempRect == aoi.rect)
                    {
                        aoiList.Remove(aoi);
                        rectangles.Remove(rectangles[rectangles.IndexOf(selectedRect)]);
                        panelAOIEdit.Hide();
                        selectedRect = new Rectangle();
                        pictureBox.Refresh();
                        return;
                    }
                }
            }
            else
            {
                rectangles.Remove(rectangles[rectangles.IndexOf(selectedRect)]);
                panelAOIEdit.Hide();
                selectedRect = new Rectangle();
                pictureBox.Refresh();
            }           
        }

        #endregion
        #region Event Handling

        void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (_rectMode)
            {
                isMouseDown = true;

                startXY = e.Location;
            }
            else
            {
                for (int i = 1; i < 6; i++)
                {
                    if (GetHandleRect(i, selectedRect).Contains(e.Location))
                    {
                        dragHandle = i;
                        tempRect = selectedRect;
                        dragPoint = GetHandlePoint(i, selectedRect);
                    }
                }
            }
        }

        private void AddAOItoList()
        {
            if (checkBoxInbound.Checked && checkBoxOutbound.Checked)
            {
                aoiList.Add(new AOI()
                {
                    rect = selectedRect,
                    oneDirection = "both-directions"
                });
            }
            else if (checkBoxInbound.Checked)
            {
                aoiList.Add(new AOI()
                {
                    rect = selectedRect,
                    oneDirection = "inbound"
                });
            }
            else if (checkBoxOutbound.Checked)
            {
                aoiList.Add(new AOI()
                {
                    rect = selectedRect,
                    oneDirection = "outbound"
                });
            }
            else if (!checkBoxInbound.Checked && !checkBoxOutbound.Checked)
            {
                aoiList.Add(new AOI()
                {
                    rect = selectedRect,
                    oneDirection = "both-directions"
                });
            }
        }

        private void SaveConfig(Item myItem)
        {
            VehicleDetector tempVehicleDetector = new VehicleDetector(myItem);
            if (camConfig != null)
            {

                camConfig.name = textBoxName.Text;
                camConfig.host = _selectedCameraItem.FQID.ServerId.ServerHostname.ToString();
                camConfig.serverIP = textBoxServerIP.Text;
                camConfig.username = textBoxUsername.Text;
                camConfig.password = textBoxPassword.Text;
                camConfig.rotation = 0;
                camConfig.inboundUp = checkBoxInboundUp.Checked;
                camConfig.guid = _selectedCameraItem.FQID.ObjectId.ToString();
                camConfig.fps = 9;
                camConfig.sensitivity = 25;
                camConfig.areasOfInterest = aoiList;
            }
            else
            {
                camConfig = new CamConfig()
                {
                    name = textBoxName.Text,
                    host = _selectedCameraItem.FQID.ServerId.ServerHostname.ToString(),
                    serverIP = textBoxServerIP.Text,
                    username = textBoxUsername.Text,
                    password = textBoxPassword.Text,
                    rotation = 0,
                    inboundUp = checkBoxInboundUp.Checked,
                    guid = _selectedCameraItem.FQID.ObjectId.ToString(),
                    fps = 9,
                    sensitivity = 25,
                    areasOfInterest = aoiList
                };
            }
            
            tempVehicleDetector.Item.Properties["camera_config"] = JsonConvert.SerializeObject(camConfig);

            UpdateItem(tempVehicleDetector);
        }      

        private void PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (!_rectMode)
                    {
                        if (rectangles.Count > 0)
                        {
                            foreach (Rectangle r in rectangles)
                            {
                                if (r.Contains(e.Location))
                                {
                                    panelAOIEdit.Show();
                                    selectedRect = r;
                                    EditRectangle(selectedRect, e);
                                }
                            }
                        }
                    }
                    break;
                case MouseButtons.Right:

                    break;
            }
        }

        private void AddAOIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_rectMode)
                _rectMode = false;
            else
                _rectMode = true;
        }

        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (_rectMode)
            {
                if (isMouseDown)
                {
                    endXY = e.Location;
                    pictureBox.Refresh();
                }
            }
            else if (rectangles.Count > 0 && !selectedRect.IsEmpty)
            {
                Rectangle r = rectangles[rectangles.IndexOf(selectedRect)];
                int diffX = dragPoint.X - e.Location.X;
                int diffY = dragPoint.Y - e.Location.Y;
                switch (dragHandle)
                {

                    case 1:
                        r = new Rectangle(tempRect.Left, tempRect.Top - diffY, tempRect.Width, tempRect.Height + diffY);
                        break;
                    case 2:
                        r = new Rectangle(tempRect.Left - diffX, tempRect.Top, tempRect.Width + diffX, tempRect.Height);
                        break;
                    case 3:
                        r = new Rectangle(tempRect.Left, tempRect.Top, tempRect.Width, tempRect.Height - diffY);
                        break;
                    case 4:
                        r = new Rectangle(tempRect.Left, tempRect.Top, tempRect.Width - diffX, tempRect.Height);
                        break;
                    case 5:
                        r = new Rectangle(tempRect.Left - diffX, tempRect.Top - diffY, tempRect.Width, tempRect.Height);
                        break;
                }

                rectangles[rectangles.IndexOf(selectedRect)] = r;
                selectedRect = rectangles[rectangles.IndexOf(r)];

                if (dragHandle > 0)
                    pictureBox.Invalidate();
            }
        }


        private void PictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (rectangles.Count > 0)
            {
                e.Graphics.DrawRectangles(Pens.GreenYellow, rectangles.ToArray());
                if (!selectedRect.IsEmpty)
                {
                    e.Graphics.DrawRectangle(Pens.Red, rectangles[rectangles.IndexOf(selectedRect)]);
                    for (int i = 1; i < 6; i++)
                    {
                        e.Graphics.FillRectangle(Brushes.Red, GetHandleRect(i, selectedRect));
                    }
                }

            }
            if (_rectMode) e.Graphics.DrawRectangle(Pens.Red, GetRect());
        }

        void PictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (_rectMode)
            {
                if (isMouseDown)
                {
                    endXY = e.Location;
                    isMouseDown = false;

                    rectangles.Add(GetRect());
                    selectedRect = GetRect();
                    tempRect = selectedRect;
                    panelAOIEdit.Show();
                    _rectMode = false;
                }
            }
            dragHandle = 0;
        }

        private Rectangle GetRect()
        {
            rect = new Rectangle();
            rect.X = Math.Min(startXY.X, endXY.X);
            rect.Y = Math.Min(startXY.Y, endXY.Y);
            rect.Width = Math.Abs(startXY.X - endXY.X);
            rect.Height = Math.Abs(startXY.Y - endXY.Y);
            return rect;
        }

        private Rectangle EditRectangle(Rectangle rect, MouseEventArgs e)
        {
            for (int i = 1; i < 6; i++)
            {
                if (GetHandleRect(i, rect).Contains(e.Location))
                {
                    dragHandle = i;
                    tempRect = rect;
                    dragPoint = GetHandlePoint(i, rect);
                    pictureBox.Refresh();
                }
            }
            return rect;
        }


        private Rectangle GetHandleRect(int value, Rectangle r)
        {
            Point p = GetHandlePoint(value, r);
            p.Offset(-2, -2);
            return new Rectangle(p, new Size(5, 5));
        }

        private Point GetHandlePoint(int value, Rectangle r)
        {
            Point result = Point.Empty;

            if (value == 1)
                result = new Point(r.Left + (r.Width / 2), r.Top);
            else if (value == 2)
                result = new Point(r.Left, r.Top + (r.Height / 2));
            else if (value == 3)
                result = new Point(r.Left + (r.Width / 2), r.Bottom);
            else if (value == 4)
                result = new Point(r.Right, r.Top + (r.Height / 2));
            else if (value == 5)
                result = new Point(r.Left + (r.Width / 2), r.Top + (r.Height / 2));

            return result;
        }

        private void OnCameraSelect(object sender, EventArgs e)
        {
            ItemPickerForm form = new ItemPickerForm
            {
                KindFilter = Kind.Camera,
                SelectedItem = _selectedCameraItem,
                AutoAccept = true
            };

            form.Init();
            if (form.ShowDialog() == DialogResult.OK)
            {
                _selectedCameraItem = form.SelectedItem;
                buttonCameraSelect.Text = "";
                if (_selectedCameraItem != null)
                {
                    buttonCameraSelect.Text = _selectedCameraItem.Name;
                    OnUserChange(this, null);
                    if (_selectedCameraItem != null)
                    {
                        buttonCameraSelect.Text = _selectedCameraItem.Name;
                        if (_bitmapSource != null)
                        {
                            _bitmapSource.Item = _selectedCameraItem;
                            InitBitmap();
                            panelVideoPreview.Show();
                            _bitmapSource.LiveStart();
                        }
                    }
                    if (String.IsNullOrEmpty(textBoxName.Text))
                    {
                        textBoxName.Text = _selectedCameraItem.Name;
                    }
                }

            }

        }        
        #endregion 
        #region Classes
        public class CamConfig
        {
            public string name { get; set; }
            public string host { get; set; }
            public string serverIP { get; set; }
            public string username { get; set; }
            public string password { get; set; }
            public int rotation { get; set; }
            public bool inboundUp { get; set; }
            public string guid { get; set; }
            public int fps { get; set; }
            public int sensitivity { get; set; }
            public List<AOI> areasOfInterest { get; set; }
        }

        public class AOI
        {
            public Rectangle rect { get; set; }
            public string oneDirection { get; set; }
        }
        #endregion
    }
}
