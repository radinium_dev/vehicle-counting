namespace Vehicle_Counter_V2.Admin
{
    partial class Vehicle_Counter_V2UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /*protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }*/

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vehicle_Counter_V2UserControl));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonCameraSelect = new System.Windows.Forms.Button();
            this.textBoxGroup = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panelVideoPreview = new System.Windows.Forms.Panel();
            this.textBoxServerIP = new System.Windows.Forms.TextBox();
            this.checkBoxInboundUp = new System.Windows.Forms.CheckBox();
            this.panelAOIEdit = new System.Windows.Forms.Panel();
            this.buttonAOIDone = new System.Windows.Forms.Button();
            this.checkBoxOutbound = new System.Windows.Forms.CheckBox();
            this.checkBoxInbound = new System.Windows.Forms.CheckBox();
            this.buttonDeleteAOI = new System.Windows.Forms.Button();
            this.buttonSaveConfig = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addAOIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panelVideoPreview.SuspendLayout();
            this.panelAOIEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.Color.Silver;
            this.textBoxName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxName.Location = new System.Drawing.Point(119, 27);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(154, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.TextChanged += new System.EventHandler(this.OnUserChange);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(18, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Related camera:";
            // 
            // buttonCameraSelect
            // 
            this.buttonCameraSelect.BackColor = System.Drawing.Color.Gray;
            this.buttonCameraSelect.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.buttonCameraSelect.FlatAppearance.BorderSize = 4;
            this.buttonCameraSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCameraSelect.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCameraSelect.ForeColor = System.Drawing.Color.White;
            this.buttonCameraSelect.Location = new System.Drawing.Point(119, 60);
            this.buttonCameraSelect.Name = "buttonCameraSelect";
            this.buttonCameraSelect.Size = new System.Drawing.Size(154, 30);
            this.buttonCameraSelect.TabIndex = 3;
            this.buttonCameraSelect.Text = "Select camera...";
            this.buttonCameraSelect.UseVisualStyleBackColor = false;
            this.buttonCameraSelect.Click += new System.EventHandler(this.OnCameraSelect);
            // 
            // textBoxGroup
            // 
            this.textBoxGroup.BackColor = System.Drawing.Color.Silver;
            this.textBoxGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxGroup.Location = new System.Drawing.Point(119, 103);
            this.textBoxGroup.Name = "textBoxGroup";
            this.textBoxGroup.Size = new System.Drawing.Size(154, 20);
            this.textBoxGroup.TabIndex = 9;
            this.textBoxGroup.TextChanged += new System.EventHandler(this.OnUserChange);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(18, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Group:";
            // 
            // panelVideoPreview
            // 
            this.panelVideoPreview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelVideoPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVideoPreview.Controls.Add(this.textBoxServerIP);
            this.panelVideoPreview.Controls.Add(this.checkBoxInboundUp);
            this.panelVideoPreview.Controls.Add(this.panelAOIEdit);
            this.panelVideoPreview.Controls.Add(this.buttonSaveConfig);
            this.panelVideoPreview.Controls.Add(this.label3);
            this.panelVideoPreview.Controls.Add(this.pictureBox);
            this.panelVideoPreview.Location = new System.Drawing.Point(21, 200);
            this.panelVideoPreview.Name = "panelVideoPreview";
            this.panelVideoPreview.Size = new System.Drawing.Size(672, 471);
            this.panelVideoPreview.TabIndex = 10;
            // 
            // textBoxServerIP
            // 
            this.textBoxServerIP.BackColor = System.Drawing.Color.Silver;
            this.textBoxServerIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxServerIP.Location = new System.Drawing.Point(126, 4);
            this.textBoxServerIP.Name = "textBoxServerIP";
            this.textBoxServerIP.Size = new System.Drawing.Size(279, 20);
            this.textBoxServerIP.TabIndex = 36;
            // 
            // checkBoxInboundUp
            // 
            this.checkBoxInboundUp.AutoSize = true;
            this.checkBoxInboundUp.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxInboundUp.ForeColor = System.Drawing.Color.White;
            this.checkBoxInboundUp.Location = new System.Drawing.Point(6, 31);
            this.checkBoxInboundUp.Name = "checkBoxInboundUp";
            this.checkBoxInboundUp.Size = new System.Drawing.Size(84, 19);
            this.checkBoxInboundUp.TabIndex = 34;
            this.checkBoxInboundUp.Text = "InboundUp";
            this.checkBoxInboundUp.UseVisualStyleBackColor = true;
            // 
            // panelAOIEdit
            // 
            this.panelAOIEdit.BackColor = System.Drawing.Color.Gainsboro;
            this.panelAOIEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAOIEdit.Controls.Add(this.buttonAOIDone);
            this.panelAOIEdit.Controls.Add(this.checkBoxOutbound);
            this.panelAOIEdit.Controls.Add(this.checkBoxInbound);
            this.panelAOIEdit.Controls.Add(this.buttonDeleteAOI);
            this.panelAOIEdit.Location = new System.Drawing.Point(10, 65);
            this.panelAOIEdit.Name = "panelAOIEdit";
            this.panelAOIEdit.Size = new System.Drawing.Size(169, 51);
            this.panelAOIEdit.TabIndex = 33;
            // 
            // buttonAOIDone
            // 
            this.buttonAOIDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAOIDone.Location = new System.Drawing.Point(89, 24);
            this.buttonAOIDone.Name = "buttonAOIDone";
            this.buttonAOIDone.Size = new System.Drawing.Size(75, 21);
            this.buttonAOIDone.TabIndex = 3;
            this.buttonAOIDone.Text = "Done";
            this.buttonAOIDone.UseVisualStyleBackColor = true;
            this.buttonAOIDone.Click += new System.EventHandler(this.AOIDoneButton_Click);
            // 
            // checkBoxOutbound
            // 
            this.checkBoxOutbound.AutoSize = true;
            this.checkBoxOutbound.Location = new System.Drawing.Point(3, 26);
            this.checkBoxOutbound.Name = "checkBoxOutbound";
            this.checkBoxOutbound.Size = new System.Drawing.Size(73, 17);
            this.checkBoxOutbound.TabIndex = 2;
            this.checkBoxOutbound.Text = "Outbound";
            this.checkBoxOutbound.UseVisualStyleBackColor = true;
            // 
            // checkBoxInbound
            // 
            this.checkBoxInbound.AutoSize = true;
            this.checkBoxInbound.Checked = true;
            this.checkBoxInbound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxInbound.Location = new System.Drawing.Point(3, 3);
            this.checkBoxInbound.Name = "checkBoxInbound";
            this.checkBoxInbound.Size = new System.Drawing.Size(65, 17);
            this.checkBoxInbound.TabIndex = 1;
            this.checkBoxInbound.Text = "Inbound";
            this.checkBoxInbound.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteAOI
            // 
            this.buttonDeleteAOI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteAOI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteAOI.Location = new System.Drawing.Point(89, 1);
            this.buttonDeleteAOI.Name = "buttonDeleteAOI";
            this.buttonDeleteAOI.Size = new System.Drawing.Size(75, 21);
            this.buttonDeleteAOI.TabIndex = 0;
            this.buttonDeleteAOI.Text = "Delete";
            this.buttonDeleteAOI.UseVisualStyleBackColor = true;
            this.buttonDeleteAOI.Click += new System.EventHandler(this.AOIDeleteButton_Click);
            // 
            // buttonSaveConfig
            // 
            this.buttonSaveConfig.BackColor = System.Drawing.Color.Gray;
            this.buttonSaveConfig.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveConfig.ForeColor = System.Drawing.Color.White;
            this.buttonSaveConfig.Location = new System.Drawing.Point(126, 30);
            this.buttonSaveConfig.Name = "buttonSaveConfig";
            this.buttonSaveConfig.Size = new System.Drawing.Size(279, 27);
            this.buttonSaveConfig.TabIndex = 32;
            this.buttonSaveConfig.Text = "Save configuration to server";
            this.buttonSaveConfig.UseVisualStyleBackColor = false;
            this.buttonSaveConfig.Click += new System.EventHandler(this.buttonSaveConfig_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 15);
            this.label3.TabIndex = 35;
            this.label3.Text = "Server IP Address:";
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox.Location = new System.Drawing.Point(10, 65);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(638, 358);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 30;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox_Paint);
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseClick);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAOIToolStripMenuItem,
            this.cancelToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(120, 48);
            // 
            // addAOIToolStripMenuItem
            // 
            this.addAOIToolStripMenuItem.Name = "addAOIToolStripMenuItem";
            this.addAOIToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.addAOIToolStripMenuItem.Text = "Add AOI";
            this.addAOIToolStripMenuItem.Click += new System.EventHandler(this.AddAOIToolStripMenuItem_Click);
            // 
            // cancelToolStripMenuItem
            // 
            this.cancelToolStripMenuItem.Name = "cancelToolStripMenuItem";
            this.cancelToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.cancelToolStripMenuItem.Text = "Cancel";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.BackColor = System.Drawing.Color.Silver;
            this.textBoxUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxUsername.Location = new System.Drawing.Point(119, 140);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(154, 20);
            this.textBoxUsername.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(18, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "Username:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BackColor = System.Drawing.Color.Silver;
            this.textBoxPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPassword.Location = new System.Drawing.Point(119, 174);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxPassword.Size = new System.Drawing.Size(154, 20);
            this.textBoxPassword.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(18, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "Password:";
            // 
            // Vehicle_Counter_V2UserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxUsername);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panelVideoPreview);
            this.Controls.Add(this.textBoxGroup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonCameraSelect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label1);
            this.Name = "Vehicle_Counter_V2UserControl";
            this.Size = new System.Drawing.Size(743, 705);
            this.panelVideoPreview.ResumeLayout(false);
            this.panelVideoPreview.PerformLayout();
            this.panelAOIEdit.ResumeLayout(false);
            this.panelAOIEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCameraSelect;
        private System.Windows.Forms.TextBox textBoxGroup;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelVideoPreview;
        private System.Windows.Forms.TextBox textBoxServerIP;
        private System.Windows.Forms.CheckBox checkBoxInboundUp;
        private System.Windows.Forms.Panel panelAOIEdit;
        private System.Windows.Forms.Button buttonAOIDone;
        private System.Windows.Forms.CheckBox checkBoxOutbound;
        private System.Windows.Forms.CheckBox checkBoxInbound;
        private System.Windows.Forms.Button buttonDeleteAOI;
        private System.Windows.Forms.Button buttonSaveConfig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addAOIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label6;
    }
}
