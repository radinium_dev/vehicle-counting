using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using Cygnetic.MIPPluginCommons.Licence;

namespace Vehicle_Counter_V2.Admin
{
    public partial class HelpPage : ItemNodeUserControl
    {
        /// <summary>
        /// User control to display help page
        /// </summary>	
        public HelpPage()
        {
            InitializeComponent();
            /*
            licensingUserControl1 = new LicensingUserControl(Vehicle_Counter_V2Definition.LicenceValidator);
            this.licensingUserControl1.Location = new System.Drawing.Point(6, 6);
            this.licensingUserControl1.Name = "licensingUserControl1";
            this.licensingUserControl1.Size = new System.Drawing.Size(461, 517);
            this.licensingUserControl1.TabIndex = 32;

            licensingUserControl1.ApiKey = Utility.ReadLicensingApiKey();
            //licensingUserControl1.LicenceRequestType = RadiniumClickPluginDefinition.LicenceRequestType;

            licensingUserControl1.Licencables = new List<Licencable>();

            //licensingUserControl1.Licencables.AddRange(entries.Select((entry) => new Licencable(entry.hostname, entry.fingerprint)));

            this.Licensing.Controls.Add(this.licensingUserControl1);
            licensingUserControl1.Update();*/
        }

        /// <summary>
        /// Display information from or about the Item selected.
        /// </summary>
        /// <param name="item"></param>
        public override void Init(Item item)
        {

        }

        /// <summary>
        /// Close any session and release any resources used.
        /// </summary>
        public override void Close()
        {

        }

        private void About_Click(object sender, EventArgs e)
        {

        }
    }
}
