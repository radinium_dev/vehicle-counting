﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Xml;
using Vehicle_Counter_V2;
using VideoOS.Platform;
using MessageBox = System.Windows.MessageBox;

namespace Vehicle_Counter_V2.Admin
{
    class Utility
    {
        public static void SaveConfiguration(string licence)
        {
            //var fingerprint = new RadiniumClickFingerprintUtil().GenerateFingerprint(null);
            var hostname = Environment.MachineName;
            var options = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            if (options != null)
            {
                IEnumerator children = options.ChildNodes.GetEnumerator();
                bool found = false;
                while (children.MoveNext())
                {
                    XmlNode option = children.Current as XmlNode;
                    if (option != null && option.Name == "licences")
                    {
                        option.InnerText = licence;
                        found = true;
                    }
                }
                if (!found)
                {
                    XmlNode licencesNode = options.OwnerDocument.CreateElement("licences");
                    licencesNode.InnerText = licence;
                    options.AppendChild(licencesNode);
                }
                Configuration.Instance.SaveOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false, options);
            }

            MessageBox.Show("Your configuration has been saved.");
        }

        public class Entry
        {
            public string hostname { get; set; }
            public string fingerprint { get; set; }
        }

        internal static List<Entry> GetEntries()
        {
            List<Entry> hostnames = new List<Entry>();
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            if (node != null)
            {
                IEnumerator children = node.ChildNodes.GetEnumerator();
                while (children.MoveNext())
                {
                    XmlNode option = children.Current as XmlNode;
                    if (option == null || option.Name != "entry")
                    {
                        continue;
                    }
                    else
                    {
                        hostnames.Add(new Entry { hostname = option.ChildNodes[1].InnerText, fingerprint = option.ChildNodes[0].InnerText });
                    }
                }

                return hostnames;
            }
            else
            {
                return null;
            }
        }

        public static string ReadLicence()
        {
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            if (node != null)
            {
                try
                {
                    return node.SelectSingleNode("//licences").InnerText;
                }
                catch (Exception) { }
            }
            return "";
        }

        public static string ReadLicensingApiKey()
        {

            XmlNode node = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            if (node != null)
            {
                try
                {
                    return node.SelectSingleNode("//licensingApiKey").InnerText;
                }
                catch (Exception) { }
            }
            return "";
        }
    }
}
