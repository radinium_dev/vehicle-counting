
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Vehicle_Counter_V2;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.UI;

namespace Cygnetic.VehicleDetect.Admin
{
	public partial class VehicleDetectorOverviewControl : ItemNodeUserControl
	{
       
        public VehicleDetectorOverviewControl()
		{
			InitializeComponent();
            FillContent();

		}

		internal String DatabaseName
		{
			get { return textBoxDatabaseName.Text; }
			set { textBoxDatabaseName.Text = value; }
		}

        internal string DatabaseHost
        {
            get { return textBoxDatabaseHost.Text; }
            set { textBoxDatabaseHost.Text = value; }
        }

        internal string Username
        {
            get { return textBoxUsername.Text; }
            set { textBoxUsername.Text = value; }
        }

        internal string Password
        {
            get { return textBoxPassword.Text; }
            set { textBoxPassword.Text = value; }
        }

        internal string ReportAddress
        {
            get { return textBoxAddress.Text; }
            set { textBoxAddress.Text = value; }
        }

    
		internal void FillContent()
		{
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            DatabaseName = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_NAME, node);
            DatabaseHost = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_HOST, node);
            Username = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_USERNAME, node);
            Password = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_DATABASE_PASSWORD, node);
            ReportAddress = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_REPORT_ADDRESS, node);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
            if (Username != "")
            {
                connectionString.UserID = Username;
                connectionString.Password = Password;
            }
            else
            {
                connectionString.IntegratedSecurity = true;
            }
            connectionString.ConnectTimeout = 5;
            connectionString.DataSource = DatabaseHost;
            connectionString.InitialCatalog = DatabaseName;

            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionString.ConnectionString;
            try
            {
                connection.Open();
                connection.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Reason for not connecting: " + ex.Message, "Could not establish connection to database!");
            }

            var tempConfig = readConfigXml();
            XmlNode catalogNode = null;
            XmlNode hostNode = null;
            XmlNode userNode = null;
            XmlNode passwordNode = null;

            if (tempConfig != null)
            {
                try
                {
                    catalogNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_NAME);
                    hostNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_HOST);
                    userNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_USERNAME);
                    passwordNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_PASSWORD);
                }
                catch (Exception)
                {
                }
            }

            if (catalogNode == null || hostNode == null || userNode == null || passwordNode == null)
            {
                tempConfig = createConfig();
                catalogNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_NAME);
                hostNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_HOST);
                userNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_USERNAME);
                passwordNode = tempConfig.SelectSingleNode("//" + Vehicle_Counter_V2Definition.OPTION_DATABASE_PASSWORD);
            }

            //DBName                    
            catalogNode.InnerText = textBoxDatabaseName.Text;
            //HostName                      
            hostNode.InnerText = textBoxDatabaseHost.Text;
            //UserName                      
            userNode.InnerText = textBoxUsername.Text;
            //Password  
            passwordNode.InnerText = textBoxPassword.Text;

            saveConfigXml(tempConfig);
            
            MessageBox.Show("Successfully saved configuration!", "Test succeeded!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                image = new Bitmap(openFileDialog1.FileName);

                image = (Bitmap)resizeImage(image, new Size(95, 95));
                pictureBox1.Width = 95;
                pictureBox1.Height = 95;
                pictureBox1.Dock = DockStyle.None;
                pictureBox1.Image = (Image)image;
            }
        }
        public static Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        Bitmap image;
       
        void UpdateConfigOption(string configOption, string newValue)
        {
            var tempConfig = readConfigXml();
            if(tempConfig == null)
            {
                createConfig();
                tempConfig = readConfigXml();
            }
            XmlNode node = tempConfig.SelectSingleNode("//" + configOption);
            if(node == null)
            {
                node = tempConfig.OwnerDocument.CreateNode(XmlNodeType.Element, configOption, "");
                tempConfig.AppendChild(node);
            }
            node.InnerText = newValue;

            saveConfigXml(tempConfig);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string base64String = "";
            if (image != null)
            {
                using (Image image = Image.FromFile(openFileDialog1.FileName))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                }

                UpdateConfigOption(Vehicle_Counter_V2Definition.CUSTOM_LOGO, base64String);

                MessageBox.Show("Logo successfully saved in configuration", "Logo saved!");
            }
            else
            {
                MessageBox.Show("Please select a logo", "Logo not selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       

        private static XmlNode readConfigXml()
        {
            //createConfig();
            var config = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            return config;
        }

        private static XmlNode createConfig()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode newNode = doc.CreateNode(XmlNodeType.Element, "Options", "");
            newNode.AppendChild(doc.CreateNode(XmlNodeType.Element, Vehicle_Counter_V2Definition.OPTION_DATABASE_NAME, ""));
            newNode.AppendChild(doc.CreateNode(XmlNodeType.Element, Vehicle_Counter_V2Definition.OPTION_DATABASE_HOST, ""));
            newNode.AppendChild(doc.CreateNode(XmlNodeType.Element, Vehicle_Counter_V2Definition.OPTION_DATABASE_USERNAME, ""));
            newNode.AppendChild(doc.CreateNode(XmlNodeType.Element, Vehicle_Counter_V2Definition.OPTION_DATABASE_PASSWORD, ""));
            newNode.AppendChild(doc.CreateNode(XmlNodeType.Element, Vehicle_Counter_V2Definition.OPTION_REPORT_ADDRESS, ""));
            newNode.AppendChild(doc.CreateNode(XmlNodeType.Element, Vehicle_Counter_V2Definition.CUSTOM_LOGO, ""));
            saveConfigXml(newNode);
            return newNode;
        }

        private static void saveConfigXml(XmlNode newNode)
        {
            Configuration.Instance.SaveOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false, newNode);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            UpdateConfigOption(Vehicle_Counter_V2Definition.OPTION_REPORT_ADDRESS, ReportAddress);
        }
    }
}
