using Cygnetic.MIPPluginCommons.Licence;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using Vehicle_Counter_V2.Admin;
using Vehicle_Counter_V2.Client;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Background;
using VideoOS.Platform.Client;

namespace Vehicle_Counter_V2
{
    /// <summary>
    /// The PluginDefinition is the ‘entry’ point to any plugin.  
    /// This is the starting point for any plugin development and the class MUST be available for a plugin to be loaded.  
    /// Several PluginDefinitions are allowed to be available within one DLL.
    /// Here the references to all other plugin known objects and classes are defined.
    /// The class is an abstract class where all implemented methods and properties need to be declared with override.
    /// The class is constructed when the environment is loading the DLL.
    /// </summary>
    public class Vehicle_Counter_V2Definition : PluginDefinition
    {
        private static System.Drawing.Image _treeNodeImage;
        private static System.Drawing.Image _topTreeNodeImage;


        public static string OPTION_DATABASE_NAME = "database";
        public static string OPTION_DATABASE_HOST = "host";
        public static string OPTION_DATABASE_USERNAME = "username";
        public static string OPTION_DATABASE_PASSWORD = "password";
        public static string OPTION_REPORT_ADDRESS = "address";
        public static string CUSTOM_LOGO = "logo";

        internal static Guid Vehicle_Counter_V2PluginId = new Guid("56e7e42a-e6ea-448c-84e4-5de264c8c37e");
        internal static Guid Vehicle_Counter_V2Kind = new Guid("14645c1f-469d-4b04-8ee9-b61b5f71eecc");

        //CLIENT
        internal static Guid ReportPluginId = new Guid("b9d702c5-a65a-40cb-bf6f-fc4196e1851a");
        internal static Guid WorkspaceReportPluginId = new Guid("28a861bf-ce6a-4ba3-8e44-a47cbcd50b90");

        //BACKGROUND
        internal static Guid VehicleCountUpdaterPluginId = new Guid("3ef112e1-135f-42eb-9049-0e49d2cbbb2c");

        internal static Guid OptionsGuid = new Guid("6641799f-06ea-4d2c-a7b7-c02804772fe4");

        public static string getXmlOption(string xmlOption, XmlNode node)
        {
            if (node == null)
            {
                return "";
            }
            XmlNode targetNode = node.SelectSingleNode("//" + xmlOption);
            if(targetNode != null)
            {
                return targetNode.InnerText;
            }
            return "";
        }

        internal static String RadiniumClassifierPluginLicense = "Radinium Classifier";
        public static readonly string LicenceRequestType = "Classifier";

        #region Private fields

        private UserControl _treeNodeInofUserControl;

        //
        // Note that all the plugin are constructed during application start, and the constructors
        // should only contain code that references their own dll, e.g. resource load.

        private List<BackgroundPlugin> _backgroundPlugins = new List<BackgroundPlugin>();
        private List<ViewItemPlugin> _viewItemPlugins = new List<ViewItemPlugin>();
        private List<ItemNode> _itemNodes = new List<ItemNode>();
        private List<String> _messageIdStrings = new List<string>();
        private List<SecurityAction> _securityActions = new List<SecurityAction>();
        private List<WorkSpacePlugin> _workSpacePlugins = new List<WorkSpacePlugin>();

        #endregion

        #region Initialization

        /// <summary>
        /// Load resources 
        /// </summary>
        static Vehicle_Counter_V2Definition()
        {
            _treeNodeImage = Properties.Resources.DummyItem;
            _topTreeNodeImage = Properties.Resources.Server;
        }


        /// <summary>
        /// Get the icon for the plugin
        /// </summary>
        internal static Image TreeNodeImage
        {
            get { return _treeNodeImage; }
        }

        #endregion

        /// <summary>
        /// This method is called when the environment is up and running.
        /// Registration of Messages via RegisterReceiver can be done at this point.
        /// </summary>
        public override void Init()
        {
            // Populate all relevant lists with your plugins etc.
            _itemNodes.Add(new ItemNode(Vehicle_Counter_V2Kind, Guid.Empty,
                                         "Radinium Classifier", _treeNodeImage,
                                         "Radinium Classifier", _treeNodeImage,
                                         Category.Text, true,
                                         ItemsAllowed.Many,
                                         new Vehicle_Counter_V2ItemManager(Vehicle_Counter_V2Kind),
                                         null
                                         ));
            if (EnvironmentManager.Instance.EnvironmentType == EnvironmentType.SmartClient)
            {
                _workSpacePlugins.Add(new Vehicle_Counter_V2WorkSpacePlugin());
                _viewItemPlugins.Add(new Vehicle_Counter_V2WorkSpaceViewItemPlugin());
           
                /*
                RadiniumClassifierLicenceValidator licenceValidator = new RadiniumClassifierLicenceValidator();
                var fingerprint = new RadiniumClassifierFingerprintUtil().GenerateFingerprint(null);
                var hostname = Environment.MachineName;
                var options = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
                if (options != null)
                {
                    if (licenceValidator.ValidateLicence(fingerprint))
                    {
                        string appdata = Environment.GetEnvironmentVariable("APPDATA");
                        Directory.CreateDirectory(appdata + @"\Radinium");
                        var filename = appdata + @"\Radinium\" + hostname + ".txt";
                        var data = Admin.Utility.ReadLicence();
                        File.WriteAllText(filename, data);
                    }
                    else
                    {
                        MessageBox.Show("Please licence your Radinium Classifier Plugin.", "Licence not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    var children = options.ChildNodes.GetEnumerator();
                    bool found = false;
                    while (children.MoveNext())
                    {
                        var child = children.Current as XmlNode;
                        if (child.Name != "entry")
                        {
                            continue;
                        }
                        if (child.ChildNodes[1].InnerText == hostname && child.ChildNodes[0].InnerText == fingerprint)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        var doc = options.OwnerDocument;
                        XmlNode rootNode = doc.CreateNode(XmlNodeType.Element, "Root", "");
                        XmlNode nodeEntry = doc.CreateNode(XmlNodeType.Element, "entry", "");
                        XmlNode nodeFingerprint = doc.CreateNode(XmlNodeType.Element, "fingerprint", "");
                        XmlNode nodeHostname = doc.CreateNode(XmlNodeType.Element, "hostname", "");

                        nodeFingerprint.InnerText = fingerprint;
                        nodeHostname.InnerText = hostname;

                        rootNode.AppendChild(nodeEntry);
                        nodeEntry.AppendChild(nodeFingerprint);
                        nodeEntry.AppendChild(nodeHostname);
                        Configuration.Instance.SaveOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false, rootNode);
                        MessageBox.Show("Please remember to licence your Radinium Classifier Plugin.", "Licence not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    XmlNode rootNode = doc.CreateNode(XmlNodeType.Element, "Root", "");
                    XmlNode nodeEntry = doc.CreateNode(XmlNodeType.Element, "entry", "");
                    XmlNode nodeFingerprint = doc.CreateNode(XmlNodeType.Element, "fingerprint", "");
                    XmlNode nodeHostname = doc.CreateNode(XmlNodeType.Element, "hostname", "");

                    nodeFingerprint.InnerText = fingerprint;
                    nodeHostname.InnerText = hostname;

                    rootNode.AppendChild(nodeEntry);
                    nodeEntry.AppendChild(nodeFingerprint);
                    nodeEntry.AppendChild(nodeHostname);
                    Configuration.Instance.SaveOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false, rootNode);
                    MessageBox.Show("Please remember to licence your Radinium Classifier Plugin.", "Licence not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }*/
            }
        }

        /// <summary>
        /// The main application is about to be in an undetermined state, either logging off or exiting.
        /// You can release resources at this point, it should match what you acquired during Init, so additional call to Init() will work.
        /// </summary>
        public override void Close()
        {
            _itemNodes.Clear();
            _viewItemPlugins.Clear();
            _backgroundPlugins.Clear();
            _workSpacePlugins.Clear();
        }

        /// <summary>
        /// Return any new messages that this plugin can use in SendMessage or PostMessage,
        /// or has a Receiver set up to listen for.
        /// The suggested format is: "YourCompany.Area.MessageId"
        /// </summary>
        public override List<string> PluginDefinedMessageIds
        {
            get
            {
                return _messageIdStrings;
            }
        }

        /// <summary>
        /// If authorization is to be used, add the SecurityActions the entire plugin 
        /// would like to be available.  E.g. Application level authorization.
        /// </summary>
        public override List<SecurityAction> SecurityActions
        {
            get
            {
                return _securityActions;
            }
            set
            {
            }
        }

        #region Identification Properties

        /// <summary>
        /// Gets the unique id identifying this plugin component
        /// </summary>
        public override Guid Id
        {
            get
            {
                return Vehicle_Counter_V2PluginId;
            }
        }

        /// <summary>
        /// This Guid can be defined on several different IPluginDefinitions with the same value,
        /// and will result in a combination of this top level ProductNode for several plugins.
        /// Set to Guid.Empty if no sharing is enabled.
        /// </summary>
        public override Guid SharedNodeId
        {
            get
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Define name of top level Tree node - e.g. A product name
        /// </summary>
        public override string Name
        {
            get { return "Radinium Classifier"; }
        }

        /// <summary>
        /// Your company name
        /// </summary>
        public override string Manufacturer
        {
            get
            {
                return "Radinim (Pty) Ltd";
            }
        }

        /// <summary>
        /// Version of this plugin.
        /// </summary>
        public override string VersionString
        {
            get
            {
                return "2.3.0.1";
            }
        }

        /// <summary>
        /// Icon to be used on top level - e.g. a product or company logo
        /// </summary>
        public override System.Drawing.Image Icon
        {
            get { return _topTreeNodeImage; }
        }

        #endregion


        #region Administration properties

        /// <summary>
        /// A list of server side configuration items in the administrator
        /// </summary>
        public override List<ItemNode> ItemNodes
        {
            get { return _itemNodes; }
        }

        /// <summary>
        /// A user control to display when the administrator clicks on the top TreeNode
        /// </summary>
        public override UserControl GenerateUserControl()
        {
            _treeNodeInofUserControl = new HelpPage();
            return _treeNodeInofUserControl;
        }

        /// <summary>
        /// This property can be set to true, to be able to display your own help UserControl on the entire panel.
        /// When this is false - a standard top and left side is added by the system.
        /// </summary>
        public override bool UserControlFillEntirePanel
        {
            get { return true; }
        }
        #endregion

        #region Client related methods and properties

        /// <summary>
        /// A list of Client side definitions for Smart Client
        /// </summary>
        public override List<ViewItemPlugin> ViewItemPlugins
        {
            get { return _viewItemPlugins; }
        }


        /// <summary>
        /// Return the workspace plugins
        /// </summary>
        public override List<WorkSpacePlugin> WorkSpacePlugins
        {
            get { return _workSpacePlugins; }
        }


        /// <summary>
        /// An extension plug-in running in the Smart Client to provide extra view layouts.
        /// </summary>
        public override List<ViewLayout> ViewLayouts
        {
            get { return new List<ViewLayout>(); }
        }

        #endregion


        /// <summary>
        /// Create and returns the background task.
        /// </summary>
        public override List<BackgroundPlugin> BackgroundPlugins
        {
            get { return _backgroundPlugins; }
        }

       // internal static LicenceValidator LicenceValidator = new RadiniumClassifierLicenceValidator();

    }
    /*
    public class RadiniumClassifierLicenceValidator : LicenceValidator
    {
        public RadiniumClassifierLicenceValidator() : base(typeof(Vehicle_Counter_V2Definition), new RadiniumClassifierFingerprintUtil(), Vehicle_Counter_V2Definition.Vehicle_Counter_V2PluginId, Vehicle_Counter_V2Definition.RadiniumClassifierPluginLicense)
        {
        }

        public override string RetrieveLicenceConfiguration()
        {
            return Admin.Utility.ReadLicence();
        }

        public override void SaveLicenceConfiguration(string licenseNode)
        {
            Admin.Utility.SaveConfiguration(licenseNode);
        }
    }*/
}
