using VideoOS.Platform.Client;

namespace Vehicle_Counter_V2.Client
{
    public class Vehicle_Counter_V2WorkSpaceViewItemManager : ViewItemManager
    {
        public Vehicle_Counter_V2WorkSpaceViewItemManager() : base("Vehicle_Counter_V2WorkSpaceViewItemManager")
        {
        }
        /*
        public override ViewItemUserControl GenerateViewItemUserControl()
        {
            //return base.GenerateViewItemUserControl();
        }*/
        
        public override ViewItemWpfUserControl GenerateViewItemWpfUserControl()
        {
            return new Vehicle_Counter_V2WorkSpaceViewItemWpfUserControl();
        }
    }
}
