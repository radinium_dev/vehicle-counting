using OxyPlot;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;
using System.Xml;
using Vehicle_Counter_V2.Data;
using Vehicle_Counter_V2.Utility;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using System.Linq;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.IO;
using System.Reflection;
using System.Drawing;
using PdfSharp.Pdf.IO;
using System.Windows.Forms;
using System.Diagnostics;
using OxyPlot.Series;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using System.Windows.Media.Imaging;
using OxyPlot.Wpf;
using PlotCommands = OxyPlot.PlotCommands;
using MigraDoc.DocumentObjectModel.Tables;
using PieSeries = OxyPlot.Series.PieSeries;

namespace Vehicle_Counter_V2.Client
{
    public class CheckedData
    {
        public string Name { get; set; }
        public bool Checked { get; set; }
        public string GUID { get; set; }
        public string Group { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Interaction logic for Vehicle_Counter_V2WorkSpaceViewItemWpfUserControl.xaml
    /// </summary>
    public partial class Vehicle_Counter_V2WorkSpaceViewItemWpfUserControl : ViewItemWpfUserControl
    {
        volatile bool running = true;
        private ImageViewerWpfControl vc = null;
        string inboundOutbound;

        Boolean motorbikeHidden, truckHidden, carHidden, busHidden = false;

        /* private PlotModel modelP1;
        public PlotModel Model1
        {
            get { return modelP1; }
            set { modelP1 = value; }
        }*/
        public PlotModel percentagePieChartModel { get; private set; }
        private PieSeries cameraVehicleDistribution { get; set; }
        public Vehicle_Counter_V2WorkSpaceViewItemWpfUserControl()
        {
            customController = new PlotController();
            customController.UnbindMouseDown(OxyMouseButton.Left);
            customController.BindMouseEnter(PlotCommands.HoverSnapTrack);

            percentagePieChartModel = new PlotModel { Title = "Traffic distribution per camera", PlotAreaBackground = OxyColors.Black };
           
            InitializeComponent();
        }

        public string[] periodStr =
        {
            "Today",
            "Yesterday",
            "Last 7 days",
            "Last week",
            "Last 30 days",
            "Last month",
            "Last 365 days",
            "Custom"
        };

        public PlotController customController;

        public override void Init()
        {
            //ThreadPool.QueueUserWorkItem(new WaitCallback(cameraStreamThread));
            /*modelP1 = new PlotModel { Title = "Pie Sample1" };

            var seriesP1 = new PieSeries { StrokeThickness = 2.0, InsideLabelPosition = 0.8, AngleSpan = 360, StartAngle = 0 };

            seriesP1.Slices.Add(new PieSlice("Africa", 1030) { IsExploded = false, Fill = OxyColors.PaleVioletRed });
            seriesP1.Slices.Add(new PieSlice("Americas", 929) { IsExploded = true });
            seriesP1.Slices.Add(new PieSlice("Asia", 4157) { IsExploded = true });
            seriesP1.Slices.Add(new PieSlice("Europe", 739) { IsExploded = true });
            seriesP1.Slices.Add(new PieSlice("Oceania", 35) { IsExploded = true });

            modelP1.Series.Add(seriesP1);*/

            barChartIncoming.ActualController.UnbindMouseDown(OxyMouseButton.Left);
            barChartIncoming.ActualController.BindMouseEnter(PlotCommands.HoverSnapTrack);
            barChartOutgoing.ActualController.UnbindMouseDown(OxyMouseButton.Left);
            barChartOutgoing.ActualController.BindMouseEnter(PlotCommands.HoverSnapTrack);

            comboBoxPeriodSelection.ItemsSource = periodStr;

            List<Item> vehicleDetectorItems = Configuration.Instance.GetItemConfigurations(Vehicle_Counter_V2Definition.Vehicle_Counter_V2PluginId, null, Vehicle_Counter_V2Definition.Vehicle_Counter_V2Kind);
            
            LinearCountAxisInbound.LabelFormatter = _formatter;
            LinearCountAxisOutbound.LabelFormatter = _formatter;

            var vehicleDetectors = (from vdi in vehicleDetectorItems
                                select new VehicleDetector(vdi));
            vehicleDetectors.OrderBy(x => x.Name).Where(x => x.CameraItem != null).Select(x => new CheckedData() { Checked = true, Name = x.Name, Group = x.Group, GUID = x.CameraItem.FQID.ObjectId.ToString() }).ToList().ForEach(y => CameraList.Add(y));
            cameraSelectBox.ItemsSource = vehicleDetectors.OrderBy(x => x.Name);
            listCameras.ItemsSource = CameraList.OrderBy(x => x.Name);
            GroupList = vehicleDetectors.Select(x => x.Group).Distinct().Select(x => new CheckedData() { Checked = true, Name = x }).ToList();
            listGroup.ItemsSource = GroupList.OrderBy(x => x.Name);

            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);
            dispatcherTimer.Start();

            comboBoxPeriodSelection.SelectedIndex = 0;
            UpdateReportClick(null, null);
            labelInboundMotorbike.Content = "0";
            labelInboundCar.Content = "0";
            labelInboundTruck.Content = "0";
            labelInboundBus.Content = "0";
            labelOutboundMotorbike.Content = "0";
            labelOutboundCar.Content = "0";
            labelOutboundTruck.Content = "0";
            labelOutboundBus.Content = "0";


            originalLabelFormat = OutboundCarCountSeries.LabelFormatString;
            OutboundCarCountSeries.LabelFormatString = "";
            OutboundBusCountSeries.LabelFormatString = "";
            OutboundTruckCountSeries.LabelFormatString = "";
            OutboundMotorbikeCountSeries.LabelFormatString = "";
            InboundCarCountSeries.LabelFormatString = "";
            InboundBusCountSeries.LabelFormatString = "";
            InboundTruckCountSeries.LabelFormatString = "";
            InboundMotorbikeCountSeries.LabelFormatString = "";
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (cameraSelectBox.SelectedIndex < 0)
            {
                return;
            }
            object f = cameraSelectBox.SelectedItem;
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                timerUpdateCounts_Tick(f, e);
            }).Start();
        }

        public override void Close()
        {
            running = false;
        }


        /// <summary>
        /// Do not show the sliding toolbar!
        /// </summary>
        public override bool ShowToolbar
        {
            get { return false; }
        }

        private void ViewItemWpfUserControl_ClickEvent(object sender, EventArgs e)
        {
            FireClickEvent();
        }

        private void ViewItemWpfUserControl_DoubleClickEvent(object sender, EventArgs e)
        {
            FireDoubleClickEvent();
        }

        public static string _blankFormatter(double d)
        {
            return "";
        }

        public static string _formatter(double d)
        {
            if (d < 1E3)
            {
                return String.Format("{0}", d);
            }
            else if (d >= 1E3 && d < 1E6)
            {
                return String.Format("{0}K", d / 1E3);
            }
            else if (d >= 1E6 && d < 1E9)
            {
                return String.Format("{0}M", d / 1E6);
            }
            else if (d >= 1E9)
            {
                return String.Format("{0}B", d / 1E9);
            }
            else
            {
                return String.Format("{0}", d);
            }
        }

        /**********
         * Grabs the values per camera
         **********/
        private Dictionary<string, Dictionary<string, KeyValuePair<int, int>>> GetPerCameraCounts()
        {

            var connection = SqlUtility.GetSqlConnection();
            if (connection == null)
            {
                System.Windows.Forms.MessageBox.Show("Database connection not set!  Please configure in the management client.");
                return null;
            }
            using (connection)
            {
                using (var context = new VehicleDetectContext(connection, contextOwnsConnection: false))
                {
                    List<string> cameraIdList = new List<string>();
                    foreach (CheckedData item in CameraList.Where(x => x.Checked))
                    {
                        string itmString = item.GUID;
                        cameraIdList.Add(itmString);
                    }
                    string[] cameraIds = cameraIdList.ToArray();


                    DateTime StartDate = StartDatePicker.SelectedDate.GetValueOrDefault();
                    DateTime EndDate = EndDatePicker.SelectedDate.GetValueOrDefault().AddDays(1).AddMilliseconds(-1);

                    DateTime universalTimeStart = StartDate.ToUniversalTime();
                    DateTime universalTimeEnd = EndDate.ToUniversalTime();

                    var where = context.VehicleCountSummary.Where(vc => cameraIds.Any(y => y == vc.CameraItemID) &&
                                        vc.StartTime >= universalTimeStart && vc.StartTime < universalTimeEnd && vc.SecondsTimespan == 3600
                                        && (inboundOutbound == "Inbound" ? vc.Inbound : inboundOutbound == "Outbound" ? !vc.Inbound : true)).ToList();

                    //Get total days for determining group by clause
                    int totalDays = (int)(Math.Ceiling((EndDate - StartDate).TotalDays)); //round up
                    IQueryable<IGrouping<DateTime, VehicleCountSummary>> groupedInbound, groupedOutbound;
                    Dictionary<DateTime, int>[] carCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals
                    Dictionary<DateTime, int>[] busCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals
                    Dictionary<DateTime, int>[] truckCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals
                    Dictionary<DateTime, int>[] motorbikeCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals



                    string stringFormat = "HH:mm";

                    if (totalDays < 2) //Group by hour of day if only 1 day
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "15";
                                                barChart.Series[1]["PixelPointWidth"] = "15";
                                                barChart.Series[2]["PixelPointWidth"] = "15";*/
                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, vc.StartTime.Hour, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, vc.StartTime.Hour, 0, 0));
                        stringFormat = "HH:mm";

                        //Initialize counts
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(24);
                            busCounts[i] = new Dictionary<DateTime, int>(24);
                            truckCounts[i] = new Dictionary<DateTime, int>(24);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(24);
                            for (int j = 0; j < 24; j++)
                            {
                                carCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                                busCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                                truckCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                                motorbikeCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                            }
                        }
                    }
                    else if (totalDays < 8) //Group by day of week
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "40";
                                                barChart.Series[1]["PixelPointWidth"] = "40";
                                                barChart.Series[2]["PixelPointWidth"] = "40";*/
                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        //                        barChart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM/yyyy (ddd)";
                        //Initialize counts
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(7);
                            busCounts[i] = new Dictionary<DateTime, int>(7);
                            truckCounts[i] = new Dictionary<DateTime, int>(7);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(7);
                            DateTime start = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0);
                            for (int j = 0; j < totalDays; j++)
                            {
                                carCounts[i][start.AddDays(j)] = 0;
                                busCounts[i][start.AddDays(j)] = 0;
                                truckCounts[i][start.AddDays(j)] = 0;
                                motorbikeCounts[i][start.AddDays(j)] = 0;
                            }
                        }
                        stringFormat = "MM/dd";
                    }
                    else if (totalDays < 32) //Group by day of month
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "15";
                                                barChart.Series[1]["PixelPointWidth"] = "15";
                                                barChart.Series[2]["PixelPointWidth"] = "15";*/
                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        //                        barChart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM/yyyy";
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            busCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            truckCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            DateTime start = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0);
                            for (int j = 0; j < totalDays; j++)
                            {
                                carCounts[i][start.AddDays(j)] = 0;
                                busCounts[i][start.AddDays(j)] = 0;
                                truckCounts[i][start.AddDays(j)] = 0;
                                motorbikeCounts[i][start.AddDays(j)] = 0;
                            }
                        }
                        stringFormat = "MM/dd";
                    }
                    else //Group by month of year
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "30";
                                                barChart.Series[1]["PixelPointWidth"] = "30";
                                                barChart.Series[2]["PixelPointWidth"] = "30";*/
                        int totalMonths = (int)Math.Ceiling(totalDays / (365.2425 / 12));

                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, 1, 0, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, 1, 0, 0, 0));

                        //                        barChart.ChartAreas[0].AxisX.LabelStyle.Format = "MMMM yyyy";
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            busCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            truckCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            DateTime start = new DateTime(StartDate.Year, StartDate.Month, 1, 0, 0, 0);
                            for (int j = 0; j < totalMonths; j++)
                            {
                                carCounts[i][start.AddMonths(j)] = 0;
                                busCounts[i][start.AddMonths(j)] = 0;
                                truckCounts[i][start.AddMonths(j)] = 0;
                                motorbikeCounts[i][start.AddMonths(j)] = 0;
                            }
                        }
                        stringFormat = "yyyy/MM";
                    }

                    Dictionary<string, Dictionary<string, KeyValuePair<int, int>>> returnDict = new Dictionary<string, Dictionary<string, KeyValuePair<int, int>>>();

                    foreach (CheckedData item in CameraList.Where(x => x.Checked))
                    {
                        var resultsInboundCar = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && (y.ObjectName == "car" || y.ObjectName == null)).Sum(x => x.Count) });
                        var resultsOutboundCar = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && (y.ObjectName == "car" || y.ObjectName == null)).Sum(x => x.Count) });
                        var resultsInboundTruck = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && y.ObjectName == "truck").Sum(x => x.Count) });
                        var resultsOutboundTruck = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && y.ObjectName == "truck").Sum(x => x.Count) });
                        var resultsInboundBus = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && y.ObjectName == "bus").Sum(x => x.Count) });
                        var resultsOutboundBus = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && y.ObjectName == "bus").Sum(x => x.Count) });
                        var resultsInboundMotorbike = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && y.ObjectName == "motorbike").Sum(x => x.Count) });
                        var resultsOutboundMotorbike = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.CameraItemID == item.GUID && y.ObjectName == "motorbike").Sum(x => x.Count) });

                        //Populate counts from results
                        foreach (var result in resultsInboundCar)
                        {
                            carCounts[0][result.Date] = result.Count;
                            carCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsOutboundCar)
                        {
                            carCounts[1][result.Date] = result.Count;
                            carCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsInboundTruck)
                        {
                            truckCounts[0][result.Date] = result.Count;
                            truckCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsOutboundTruck)
                        {
                            truckCounts[1][result.Date] = result.Count;
                            truckCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsInboundBus)
                        {
                            busCounts[0][result.Date] = result.Count;
                            busCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsOutboundBus)
                        {
                            busCounts[1][result.Date] = result.Count;
                            busCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsInboundMotorbike)
                        {
                            motorbikeCounts[0][result.Date] = result.Count;
                            motorbikeCounts[2][result.Date] = 0;
                        }

                        foreach (var result in resultsOutboundMotorbike)
                        {
                            motorbikeCounts[1][result.Date] = result.Count;
                            motorbikeCounts[2][result.Date] = 0;
                        }

                        Dictionary<string, KeyValuePair<int, int>> results = new Dictionary<string, KeyValuePair<int, int>>();
                        results.Add("Motorbike", new KeyValuePair<int, int>(motorbikeCounts[0].Sum(y => y.Value), motorbikeCounts[1].Sum(y => y.Value)));
                        results.Add("Truck", new KeyValuePair<int, int>(truckCounts[0].Sum(y => y.Value), truckCounts[1].Sum(y => y.Value)));
                        results.Add("Car", new KeyValuePair<int, int>(carCounts[0].Sum(y => y.Value), carCounts[1].Sum(y => y.Value)));
                        results.Add("Bus", new KeyValuePair<int, int>(busCounts[0].Sum(y => y.Value), busCounts[1].Sum(y => y.Value)));
                        returnDict.Add(item.Name, results);
                    }
                    return returnDict;
                }
            }

            return null;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            var connection = SqlUtility.GetSqlConnection();
            if (connection == null)
            {
                System.Windows.Forms.MessageBox.Show("Database connection not set!  Please configure in the management client.");
                return;
            }
            using (connection)
            {
                using (var context = new VehicleDetectContext(connection, contextOwnsConnection: false))
                {
                    //TotalCount.Clear();
                    InboundCountCar.Clear();
                    OutboundCountCar.Clear();
                    InboundCountBus.Clear();
                    OutboundCountBus.Clear();
                    InboundCountTruck.Clear();
                    OutboundCountTruck.Clear();
                    InboundCountMotorbike.Clear();
                    OutboundCountMotorbike.Clear();
                    List<string> cameraIdList = new List<string>();
                    foreach (CheckedData item in CameraList.Where(x => x.Checked))
                    {
                        string itmString = item.GUID;
                        cameraIdList.Add(itmString);
                    }
                    string[] cameraIds = cameraIdList.ToArray();

                    DateTime StartDate = StartDatePicker.SelectedDate.GetValueOrDefault();
                    DateTime EndDate = EndDatePicker.SelectedDate.GetValueOrDefault().AddDays(1).AddMilliseconds(-1);

                    DateTime universalTimeStart = StartDate.ToUniversalTime();
                    DateTime universalTimeEnd = EndDate.ToUniversalTime();
                    //string inboundOutbound = listInboundOutbound.SelectedItem.ToString();

                    var where = context.VehicleCountSummary.Where(vc => cameraIds.Any(y => y == vc.CameraItemID) &&
                                                            vc.StartTime >= universalTimeStart && vc.StartTime < universalTimeEnd && vc.SecondsTimespan == 3600
                                                            && (inboundOutbound == "Inbound" ? vc.Inbound : inboundOutbound == "Outbound" ? !vc.Inbound : true)).ToList();
                    where.ForEach(x => x.StartTime = x.StartTime.ToLocalTime());

                    //Get total days for determining group by clause
                    int totalDays = (int)(Math.Ceiling((EndDate - StartDate).TotalDays)); //round up
                    IQueryable<IGrouping<DateTime, VehicleCountSummary>> groupedInbound, groupedOutbound;
                    Dictionary<DateTime, int>[] carCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals
                    Dictionary<DateTime, int>[] busCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals
                    Dictionary<DateTime, int>[] truckCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals
                    Dictionary<DateTime, int>[] motorbikeCounts = new Dictionary<DateTime, int>[3]; //We only accomodate in/out/totals

                    string stringFormat = "HH:mm";

                    if (totalDays < 2) //Group by hour of day if only 1 day
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "15";
                        barChart.Series[1]["PixelPointWidth"] = "15";
                        barChart.Series[2]["PixelPointWidth"] = "15";*/
                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, vc.StartTime.Hour, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, vc.StartTime.Hour, 0, 0));
                        stringFormat = "HH:mm";

                        //Initialize counts
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(24);
                            busCounts[i] = new Dictionary<DateTime, int>(24);
                            truckCounts[i] = new Dictionary<DateTime, int>(24);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(24);
                            for (int j = 0; j < 24; j++)
                            {
                                carCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                                busCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                                truckCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                                motorbikeCounts[i][new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, j, 0, 0)] = 0;
                            }
                        }
                    }
                    else if (totalDays < 8) //Group by day of week
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "40";
                        barChart.Series[1]["PixelPointWidth"] = "40";
                        barChart.Series[2]["PixelPointWidth"] = "40";*/
                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        //                        barChart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM/yyyy (ddd)";
                        //Initialize counts
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(7);
                            busCounts[i] = new Dictionary<DateTime, int>(7);
                            truckCounts[i] = new Dictionary<DateTime, int>(7);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(7);
                            DateTime start = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0);
                            for (int j = 0; j < totalDays; j++)
                            {
                                carCounts[i][start.AddDays(j)] = 0;
                                busCounts[i][start.AddDays(j)] = 0;
                                truckCounts[i][start.AddDays(j)] = 0;
                                motorbikeCounts[i][start.AddDays(j)] = 0;
                            }
                        }
                        stringFormat = "MM/dd";
                    }
                    else if (totalDays < 32) //Group by day of month
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "15";
                        barChart.Series[1]["PixelPointWidth"] = "15";
                        barChart.Series[2]["PixelPointWidth"] = "15";*/
                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, vc.StartTime.Day, 0, 0, 0));
                        //                        barChart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM/yyyy";
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            busCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            truckCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(totalDays);
                            DateTime start = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0);
                            for (int j = 0; j < totalDays; j++)
                            {
                                carCounts[i][start.AddDays(j)] = 0;
                                busCounts[i][start.AddDays(j)] = 0;
                                truckCounts[i][start.AddDays(j)] = 0;
                                motorbikeCounts[i][start.AddDays(j)] = 0;
                            }
                        }
                        stringFormat = "MM/dd";
                    }
                    else //Group by month of year
                    {
                        /*                        barChart.Series[0]["PixelPointWidth"] = "30";
                        barChart.Series[1]["PixelPointWidth"] = "30";
                        barChart.Series[2]["PixelPointWidth"] = "30";*/
                        int totalMonths = (int)Math.Ceiling(totalDays / (365.2425 / 12));

                        groupedInbound = where.Where(x => x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, 1, 0, 0, 0));
                        groupedOutbound = where.Where(x => !x.Inbound).AsQueryable().GroupBy(vc => new DateTime(vc.StartTime.Year, vc.StartTime.Month, 1, 0, 0, 0));

                        //                        barChart.ChartAreas[0].AxisX.LabelStyle.Format = "MMMM yyyy";
                        for (int i = 0; i < carCounts.Length; i++)
                        {
                            carCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            busCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            truckCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            motorbikeCounts[i] = new Dictionary<DateTime, int>(totalMonths);
                            DateTime start = new DateTime(StartDate.Year, StartDate.Month, 1, 0, 0, 0);
                            for (int j = 0; j < totalMonths; j++)
                            {
                                carCounts[i][start.AddMonths(j)] = 0;
                                busCounts[i][start.AddMonths(j)] = 0;
                                truckCounts[i][start.AddMonths(j)] = 0;
                                motorbikeCounts[i][start.AddMonths(j)] = 0;
                            }
                        }
                        stringFormat = "yyyy/MM";
                    }

                    var resultsInboundCar = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "car" || y.ObjectName == null).Sum(x => x.Count) });
                    var resultsOutboundCar = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "car" || y.ObjectName == null).Sum(x => x.Count) });
                    var resultsInboundTruck = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "truck").Sum(x => x.Count) });
                    var resultsOutboundTruck = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "truck").Sum(x => x.Count) });
                    var resultsInboundBus = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "bus").Sum(x => x.Count) });
                    var resultsOutboundBus = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "bus").Sum(x => x.Count) });
                    var resultsInboundMotorbike = groupedInbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "motorbike").Sum(x => x.Count) });
                    var resultsOutboundMotorbike = groupedOutbound.Select(g => new { Date = g.Key, Count = g.Where(y => y.ObjectName == "motorbike").Sum(x => x.Count) });

                    cameraVehicleDistribution = new PieSeries { StrokeThickness = 2.0, InsideLabelPosition = 0.8, AngleSpan = 360, StartAngle = 0, Background = OxyColors.Black };
                    percentagePieChartModel.Series.Clear();
                    List<Item> vehicleDetectorItems = Configuration.Instance.GetItemConfigurations(Vehicle_Counter_V2Definition.Vehicle_Counter_V2PluginId, null, Vehicle_Counter_V2Definition.Vehicle_Counter_V2Kind);

                    foreach (var camera in cameraIds)
                    {
                        var cameraItem = vehicleDetectorItems.Where(x => x.FQID.ObjectId.ToString() == camera).FirstOrDefault();
                        if(cameraItem == null)
                        {
                            continue;
                        }

                        cameraVehicleDistribution.Slices.Add(new PieSlice(cameraItem.Name, where.Where(x => x.CameraItemID == camera).Sum(x => x.Count)));
                    }
                    percentagePieChartModel.Series.Add(cameraVehicleDistribution);

                    if (motorbikeHidden)
                    {
                        resultsInboundMotorbike = resultsInboundMotorbike.Select(x => new { Date = x.Date, Count = 0 });
                        resultsOutboundMotorbike = resultsOutboundMotorbike.Select(x => new { Date = x.Date, Count = 0 });
                    }
                    if (carHidden)
                    {
                        resultsInboundCar = resultsInboundCar.Select(x => new { Date = x.Date, Count = 0 });
                        resultsOutboundCar = resultsOutboundCar.Select(x => new { Date = x.Date, Count = 0 });
                    }
                    if (truckHidden)
                    {
                        resultsInboundTruck = resultsInboundTruck.Select(x => new { Date = x.Date, Count = 0 });
                        resultsOutboundTruck = resultsOutboundTruck.Select(x => new { Date = x.Date, Count = 0 });
                    }
                    if (busHidden)
                    {
                        resultsInboundBus = resultsInboundBus.Select(x => new { Date = x.Date, Count = 0 });
                        resultsOutboundBus = resultsOutboundBus.Select(x => new { Date = x.Date, Count = 0 });
                    }

                    //Populate counts from results

                    foreach (var result in resultsInboundCar)
                    {
                        carCounts[0][result.Date] = result.Count;
                        carCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsOutboundCar)
                    {
                        carCounts[1][result.Date] = result.Count;
                        carCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsInboundTruck)
                    {
                        truckCounts[0][result.Date] = result.Count;
                        truckCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsOutboundTruck)
                    {
                        truckCounts[1][result.Date] = result.Count;
                        truckCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsInboundBus)
                    {
                        busCounts[0][result.Date] = result.Count;
                        busCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsOutboundBus)
                    {
                        busCounts[1][result.Date] = result.Count;
                        busCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsInboundMotorbike)
                    {
                        motorbikeCounts[0][result.Date] = result.Count;
                        motorbikeCounts[2][result.Date] = 0;
                    }

                    foreach (var result in resultsOutboundMotorbike)
                    {
                        motorbikeCounts[1][result.Date] = result.Count;
                        motorbikeCounts[2][result.Date] = 0;
                    }


                    //Add count data to chart
                    int total = 0;
                    ColumnItem dp;
                    int k = 0;
                    foreach (var count in carCounts[0])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        InboundCountCar.Add(dp);
                    }

                    total = 0;
                    k = 0;
                    foreach (var count in busCounts[0])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        InboundCountBus.Add(dp);
                    }

                    total = 0;
                    k = 0;
                    foreach (var count in truckCounts[0])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        InboundCountTruck.Add(dp);
                    }

                    total = 0;
                    k = 0;
                    foreach (var count in motorbikeCounts[0])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        InboundCountMotorbike.Add(dp);
                    }

                    InboundCarCountSeries.Title = "Inbound Car (" + total + ")";
                    InboundBusCountSeries.Title = "Inbound Bus (" + total + ")";
                    InboundTruckCountSeries.Title = "Inbound Truck (" + total + ")";
                    InboundMotorbikeCountSeries.Title = "Inbound Motorbike (" + total + ")";

                    total = 0;
                    k = 0;
                    foreach (var count in carCounts[1])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        OutboundCountCar.Add(dp);
                    }

                    total = 0;
                    k = 0;
                    foreach (var count in busCounts[1])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        OutboundCountBus.Add(dp);
                    }

                    total = 0;
                    k = 0;
                    foreach (var count in truckCounts[1])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        OutboundCountTruck.Add(dp);
                    }

                    total = 0;
                    k = 0;
                    foreach (var count in motorbikeCounts[1])
                    {
                        total += count.Value; //Add to legend total
                        dp = new ColumnItem(count.Value, k++);
                        OutboundCountMotorbike.Add(dp);
                    }

                    OutboundCarCountSeries.Title = "Outbound Car (" + total + ")";
                    OutboundBusCountSeries.Title = "Outbound Bus (" + total + ")";
                    OutboundTruckCountSeries.Title = "Outbound Truck (" + total + ")";
                    OutboundMotorbikeCountSeries.Title = "Outbound Motorbike (" + total + ")";

                    OutboundCarCountSeries.LabelPlacement = LabelPlacement.Base;
                    OutboundCarCountSeries.LabelMargin = -30;
                    OutboundBusCountSeries.LabelPlacement = LabelPlacement.Base;
                    OutboundBusCountSeries.LabelMargin = -30;
                    OutboundTruckCountSeries.LabelPlacement = LabelPlacement.Base;
                    OutboundTruckCountSeries.LabelMargin = -30;
                    OutboundMotorbikeCountSeries.LabelPlacement = LabelPlacement.Base;
                    OutboundMotorbikeCountSeries.LabelMargin = -30;

                    InboundCarCountSeries.LabelPlacement = LabelPlacement.Outside;
                    InboundCarCountSeries.LabelMargin = 14;
                    InboundBusCountSeries.LabelPlacement = LabelPlacement.Outside;
                    InboundBusCountSeries.LabelMargin = 14;
                    InboundTruckCountSeries.LabelPlacement = LabelPlacement.Outside;
                    InboundTruckCountSeries.LabelMargin = 14;
                    InboundMotorbikeCountSeries.LabelPlacement = LabelPlacement.Outside;
                    InboundMotorbikeCountSeries.LabelMargin = 14;
                    total = 0;
                    k = 0;
                    foreach (var kv in carCounts[2])
                    {
                        var val = carCounts[0][kv.Key] + carCounts[1][kv.Key];
                        dp = new ColumnItem(val, k++);
                        //TotalCount.Add(dp);
                        total += val;
                    }

                    //TotalCountSeries.Title = "Total (" + total + ")";
                    //TotalCountSeries.LabelPlacement = LabelPlacement.Outside;

                    AxesCategoryVehicleDateInbound.ItemsSource =
                    AxesCategoryVehicleDateOutbound.ItemsSource =
                    AxesCategoryVehicleDateInbound.Labels =
                    AxesCategoryVehicleDateOutbound.Labels = carCounts[0].Select(x => x.Key.ToString(stringFormat)).ToList();
                }
            }
            AxesCategoryVehicleDateInbound.Angle = -90;
            AxesCategoryVehicleDateOutbound.Angle = -90;
            //TotalCountSeries.ItemsSource = TotalCount;
            InboundCarCountSeries.ItemsSource = InboundCountCar;
            OutboundCarCountSeries.ItemsSource = OutboundCountCar;
            InboundBusCountSeries.ItemsSource = InboundCountBus;
            OutboundBusCountSeries.ItemsSource = OutboundCountBus;
            InboundTruckCountSeries.ItemsSource = InboundCountTruck;
            OutboundTruckCountSeries.ItemsSource = OutboundCountTruck;
            InboundMotorbikeCountSeries.ItemsSource = InboundCountMotorbike;
            OutboundMotorbikeCountSeries.ItemsSource = OutboundCountMotorbike;
            //TotalCountSeries.TrackerFormatString = "{1}: {2}";
            InboundCarCountSeries.TrackerFormatString = "{1}: {2}";
            OutboundCarCountSeries.TrackerFormatString = "{1}: {2}";
            InboundBusCountSeries.TrackerFormatString = "{1}: {2}";
            OutboundBusCountSeries.TrackerFormatString = "{1}: {2}";
            InboundTruckCountSeries.TrackerFormatString = "{1}: {2}";
            OutboundTruckCountSeries.TrackerFormatString = "{1}: {2}";
            InboundMotorbikeCountSeries.TrackerFormatString = "{1}: {2}";
            OutboundMotorbikeCountSeries.TrackerFormatString = "{1}: {2}";
            double[] counts = { InboundCountCar.Max(x => x.Value),
                                OutboundCountCar.Max(x => x.Value),
                                InboundCountBus.Max(x => x.Value),
                                OutboundCountBus.Max(x => x.Value),
                                InboundCountTruck.Max(x => x.Value),
                                OutboundCountTruck.Max(x => x.Value),
                                InboundCountMotorbike.Max(x => x.Value),
                                OutboundCountMotorbike.Max(x => x.Value),
            };
            double maxCount = 0;
            foreach (double count in counts)
            {
                if (maxCount < count)
                {
                    maxCount = count;
                }
            }
            /* LinearCountAxisInbound.Maximum = maxCount * 1.1;
            LinearCountAxisOutbound.Maximum = maxCount * 1.1;
            Dispatcher.InvokeAsync(() =>
            {
                barChartIncoming.ResetAllAxes();
                barChartOutgoing.ResetAllAxes();
                barChartIncoming.InvalidatePlot();
                barChartOutgoing.InvalidatePlot();
             });*/

           /* inboundCountLabel.Content = "Inbound Count: " + InboundCount.Sum(x => x.Value);
            outboundCountLabel.Content = "Outbound Count: " + OutboundCount.Sum(x => x.Value);*/
        }

        public IList<ColumnItem> TotalCount { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> OutboundCountCar { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> InboundCountCar { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> OutboundCountTruck { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> InboundCountTruck { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> OutboundCountBus { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> InboundCountBus { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> OutboundCountMotorbike { get; set; } = new List<ColumnItem>();
        public IList<ColumnItem> InboundCountMotorbike { get; set; } = new List<ColumnItem>();

        public List<CheckedData> CameraList { get; private set; } = new List<CheckedData>();
        public List<CheckedData> GroupList { get; private set; } = new List<CheckedData>();

        private void UpdateReportClick(object sender, RoutedEventArgs e)
        {
            buttonGenerate_Click(sender, null);
        }

        private void GenerateMinuteClick(object sender, RoutedEventArgs e)
        {
            DateTime StartDate = StartDatePicker.SelectedDate.GetValueOrDefault();
            DateTime EndDate = EndDatePicker.SelectedDate.GetValueOrDefault();

            DateTime universalTimeStart = StartDate.ToUniversalTime();
            DateTime universalTimeEnd = EndDate.AddDays(1).AddMilliseconds(-1).ToUniversalTime();

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "CSV Files (*.csv)|*.csv";
            dialog.AddExtension = true;
            dialog.DefaultExt = ".csv";
            string filename = ("Car_counts_" + StartDate.ToShortDateString() + "_to_" + EndDate.ToShortDateString() + "_per_minute.csv").Replace(' ', '_').Replace('/', '_');
            dialog.FileName = filename;

            DialogResult result = dialog.ShowDialog();

            List<KeyValuePair<string, string>> cameraIdList = new List<KeyValuePair<string, string>>();
            
            foreach (CheckedData item in CameraList.Where(x => x.Checked))
            {
                string itmString = item.GUID;
                cameraIdList.Add(new KeyValuePair<string, string>(item.Name, itmString));
            }
            KeyValuePair<string, string>[] cameraIds = cameraIdList.ToArray();


            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                StreamWriter fstr = File.CreateText(dialog.FileName);
                fstr.WriteLine("\"Car counts summary per minute\"");
                fstr.WriteLine("\"Date\",\"Inbound Car\",\"Outbound Car\",\"Inbound Bus\",\"Outbound Bus\",\"Inbound Truck\",\"Outbound Truck\",\"Inbound Motorbike\",\"Outbound Motorbike\", \"Camera\"");

                using (var connection = SqlUtility.GetSqlConnection())
                {
                    using (var context = new VehicleDetectContext(connection, contextOwnsConnection: false))
                    {
                        var rangeCounts = context.VehicleCounts.Where(vc => vc.CountedAt >= universalTimeStart && vc.CountedAt < universalTimeEnd).ToList();
                        for (DateTime dt = universalTimeStart; dt < universalTimeEnd; dt = dt.AddMinutes(1))
                        {
                            var minuteRange = rangeCounts.Where(x => x.CountedAt >= dt && x.CountedAt < dt.AddMinutes(1));
                            foreach(var selectedCamera in cameraIds)
                            {
                                var thisMinuteRange = minuteRange.Where(x => x.CameraItemID == selectedCamera.Value);
                                var inMotorBikes = thisMinuteRange.Where(x => x.ObjectName == "motorbike" && x.Inbound).Count();
                                var inCars = thisMinuteRange.Where(x => (x.ObjectName == "car" || x.ObjectName == null) && x.Inbound).Count();
                                var inboundTruck = thisMinuteRange.Where(x => x.ObjectName == "truck" && x.Inbound).Count();
                                var inboundBus = thisMinuteRange.Where(x => x.ObjectName == "bus" && x.Inbound).Count();
                                var outboundMotorbike = thisMinuteRange.Where(x => x.ObjectName == "motorbike" && !x.Inbound).Count();
                                var outboundCar = thisMinuteRange.Where(x => (x.ObjectName == "car" || x.ObjectName == null) && !x.Inbound).Count();
                                var outboundTruck = thisMinuteRange.Where(x => x.ObjectName == "truck" && !x.Inbound).Count();
                                var outboundBus = thisMinuteRange.Where(x => x.ObjectName == "bus" && !x.Inbound).Count();

                                string inboundcar = inCars.ToString();
                                string outboundcar = outboundCar.ToString();
                                string inboundbus = inboundBus.ToString();
                                string outboundbus = outboundBus.ToString();
                                string inboundtruck = inboundTruck.ToString();
                                string outboundtruck = outboundTruck.ToString();
                                string inboundmotorbike = inMotorBikes.ToString();
                                string outboundmotorbike = outboundMotorbike.ToString();
                                fstr.WriteLine("\"" + dt.ToLocalTime().ToString("u") +
                                    "\",\"" + inboundcar + "\",\"" + outboundcar +
                                    "\",\"" + inboundbus + "\",\"" + outboundbus +
                                    "\",\"" + inboundtruck + "\",\"" + outboundtruck +
                                    "\",\"" + inboundmotorbike + "\",\"" + outboundmotorbike + "\",\"" + selectedCamera.Key + "\"");

                            }
                        }
                    }
                }

                fstr.Flush();
                fstr.Close();
                try
                {
                    Process.Start(dialog.FileName);
                }
                catch (Exception ex) 
                {
                }
            }
        }

        private void GenerateCSVClick(object sender, RoutedEventArgs e)
        {
            DateTime StartDate = StartDatePicker.SelectedDate.GetValueOrDefault();
            DateTime EndDate = EndDatePicker.SelectedDate.GetValueOrDefault();

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "CSV Files (*.csv)|*.csv";
            dialog.AddExtension = true;
            dialog.DefaultExt = ".csv";
            string filename = ("Car_counts_" + StartDate.ToShortDateString() + "_to_" + EndDate.ToShortDateString() + ".csv").Replace(' ', '_').Replace('/', '_');
            dialog.FileName = filename;

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                StreamWriter fstr = File.CreateText(dialog.FileName);
                fstr.WriteLine("\"Car counts summary\"");
                fstr.WriteLine("\"Date\",\"Inbound Car\",\"Outbound Car\",\"Inbound Bus\",\"Outbound Bus\",\"Inbound Truck\",\"Outbound Truck\",\"Inbound Motorbike\",\"Outbound Motorbike\"");
                for (int i = 0; i < AxesCategoryVehicleDateInbound.Labels.Count; i++)
                {
                    string dateStr = AxesCategoryVehicleDateInbound.Labels[i];
                    string inboundcar = ((int)InboundCountCar[i].Value).ToString();
                    string outboundcar = ((int)OutboundCountCar[i].Value).ToString();
                    string inboundbus = ((int)InboundCountBus[i].Value).ToString();
                    string outboundbus = ((int)OutboundCountBus[i].Value).ToString();
                    string inboundtruck = ((int)InboundCountTruck[i].Value).ToString();
                    string outboundtruck = ((int)OutboundCountTruck[i].Value).ToString();
                    string inboundmotorbike = ((int)InboundCountMotorbike[i].Value).ToString();
                    string outboundmotorbike = ((int)OutboundCountMotorbike[i].Value).ToString();
                    fstr.WriteLine("\"" + dateStr + 
                        "\",\"" + inboundcar + "\",\"" + outboundcar + 
                        "\",\"" + inboundbus + "\",\"" + outboundbus + 
                        "\",\"" + inboundtruck + "\",\"" + outboundtruck +
                        "\",\"" + inboundmotorbike + "\",\"" + outboundmotorbike + "\"");
                }
                fstr.Flush();
                fstr.Close();

                try
                {
                    Process.Start(dialog.FileName);
                }
                catch (Exception ex) { }
            }
        }

        private void GeneratePDFClick(object sender, RoutedEventArgs e)
        {


            LinearCountAxisInbound.LabelFormatter = _formatter;
            LinearCountAxisOutbound.LabelFormatter = _formatter;

            DateTime StartDate = StartDatePicker.SelectedDate.GetValueOrDefault();
            DateTime EndDate = EndDatePicker.SelectedDate.GetValueOrDefault();

            DateTime universalTimeStart = StartDate.ToUniversalTime();
            DateTime universalTimeEnd = EndDate.AddDays(1).AddMilliseconds(-1).ToUniversalTime();

            buttonGenerate_Click(sender, e);
            LinearCountAxisInbound.Maximum *= (1.4 / 1.1);
            LinearCountAxisOutbound.Maximum *= (1.4 / 1.1);

            OutboundCarCountSeries.LabelFormatString =
            OutboundBusCountSeries.LabelFormatString =
            OutboundTruckCountSeries.LabelFormatString =
            OutboundMotorbikeCountSeries.LabelFormatString =
            InboundCarCountSeries.LabelFormatString =
            InboundBusCountSeries.LabelFormatString =
            InboundTruckCountSeries.LabelFormatString =
            InboundMotorbikeCountSeries.LabelFormatString = originalLabelFormat;

            Dispatcher.InvokeAsync(() =>
            {
                barChartIncoming.ResetAllAxes();
                barChartOutgoing.ResetAllAxes();
                barChartIncoming.InvalidatePlot();
                barChartOutgoing.InvalidatePlot();
            });

            XFont font = new XFont(
                "Verdana", 14, XFontStyle.BoldItalic);

            Guid guid = Guid.NewGuid();
            new List<Plot>() { barChartIncoming, barChartOutgoing }.ForEach(barChart =>
            {
                FileStream stream;
                if (barChart == barChartIncoming)
                {
                    stream = new FileStream(Path.GetTempPath() + guid.ToString() + "_in_pdfgraph.pdf", FileMode.Create);
                }
                else
                {
                    stream = new FileStream(Path.GetTempPath() + guid.ToString() + "_out_pdfgraph.pdf", FileMode.Create);
                }
                OxyPlot.Pdf.PdfExporter exporter = new OxyPlot.Pdf.PdfExporter();
                exporter.Height = 185 * 2;
                exporter.Width = 830 * 2;
                barChart.ActualModel.Series.ToList().ForEach(x =>
                {
                    x.TextColor = OxyColors.Black;
                }
                );
                barChart.ActualModel.TitleColor = OxyColors.White;
                barChart.ActualModel.PlotAreaBorderColor = OxyColors.Black;
                barChart.ActualModel.Axes.ToList().ForEach(x =>
                {
                    x.MajorGridlineColor = OxyColors.Gray;
                    x.MinorGridlineColor = OxyColors.LightGray;
                    x.MinorTicklineColor = OxyColors.LightSlateGray;
                    x.TextColor = OxyColors.Black;
                    x.TicklineColor = OxyColors.Black;
                    x.TitleColor = OxyColors.Black;
                });
                exporter.Export(barChart.ActualModel, stream);
                stream.Flush();
                stream.Close();
                barChart.ActualModel.Series.ToList().ForEach(x =>
                {
                    x.TextColor = OxyColors.White;
                }
                );
                barChart.ActualModel.Axes.ToList().ForEach(x =>
                {
                    x.MajorGridlineColor = OxyColors.Gray;
                    x.MinorGridlineColor = OxyColors.DarkGray;
                    x.MinorTicklineColor = OxyColors.BurlyWood;
                    x.TextColor = OxyColors.White;
                    x.TicklineColor = OxyColors.Black;
                    x.TitleColor = OxyColors.White;
                });
                barChart.ActualModel.TitleColor = OxyColors.White;
                barChart.ActualModel.PlotAreaBorderColor = OxyColors.White;
            });


            /*FileStream inStream = new FileStream(Path.GetTempPath() + guid.ToString() + "_out_pdfgraph.pdf", FileMode.Open);
            PdfDocument inputDocument = PdfReader.Open(inStream, PdfDocumentOpenMode.Modify);
            PdfPage pageIn = inputDocument.Pages[0];
            pageIn.Size = PdfSharp.PageSize.A4;
            pageIn.Orientation = PdfSharp.PageOrientation.Landscape;
/*            inStream = new FileStream(Path.GetTempPath() + guid.ToString() + "_in_pdfgraph.pdf", FileMode.Open);
            PdfDocument appendDocument = PdfReader.Open(inStream, PdfDocumentOpenMode.Import);
            PdfPage appendPage = appendDocument.Pages[0];
            
            PdfPage outPage = inputDocument.AddPage(appendPage);
            outPage.Size = PdfSharp.PageSize.A4;
            outPage.Orientation = PdfSharp.PageOrientation.Landscape;*/
            PdfDocument outputDocument = new PdfDocument();
            PdfPage pageIn = outputDocument.AddPage();
            pageIn.Orientation = PdfSharp.PageOrientation.Landscape;
            pageIn.Size = PdfSharp.PageSize.A4;
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            double fontCaptionHeight = font.GetHeight();
            //
            //Custom LOGO
            //
            XmlNode tempConfig = Configuration.Instance.GetOptionsConfiguration(Vehicle_Counter_V2Definition.OptionsGuid, false);
            string base64String = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.CUSTOM_LOGO, tempConfig);
            XGraphics inputGraphics = XGraphics.FromPdfPage(pageIn, XGraphicsPdfPageOptions.Append);

            XImage pdfimage = XImage.FromFile(Path.GetTempPath() + guid.ToString() + "_in_pdfgraph.pdf");
            XImage pdfoutimage = XImage.FromFile(Path.GetTempPath() + guid.ToString() + "_out_pdfgraph.pdf");
            inputGraphics.DrawImage(pdfimage, new RectangleF(0, 185.0f * 1.3f - 10.0f, (float)pdfimage.PointWidth / 2, (float)pdfimage.PointHeight / 2), new RectangleF(0, 0, (float)pdfimage.PointWidth, (float)pdfimage.PointHeight), XGraphicsUnit.Point);
            inputGraphics.DrawImage(pdfoutimage, new RectangleF(0, 185.0f * 2.256f - 10.0f, (float)pdfoutimage.PointWidth / 2, (float)pdfoutimage.PointHeight / 2), new RectangleF(0, 0, (float)pdfoutimage.PointWidth, (float)pdfoutimage.PointHeight), XGraphicsUnit.Point);

            inputGraphics.DrawString("Period: " + StartDate.ToShortDateString() + " to " + EndDate.ToShortDateString(), font, XBrushes.Black,
                new XRect(pageIn.Width * 4 / 80, fontCaptionHeight * 5, pageIn.Width * 4 / 80, fontCaptionHeight),
                XStringFormats.TopLeft);


            XFont fatfont = new XFont(
                "Verdana", 28, XFontStyle.BoldItalic);

            inputGraphics.DrawString("Vehicle Counting Report", fatfont, XBrushes.Black,
                new XRect(pageIn.Width * 4 / 80, fontCaptionHeight, pageIn.Width * 4 / 80, fatfont.GetHeight()),
                XStringFormats.TopLeft);

            Image image;

            if (base64String != null && !String.IsNullOrWhiteSpace(base64String))
            {
                byte[] bytes = Convert.FromBase64String(base64String);
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }
                inputGraphics.DrawImage(image, (float)pageIn.Width * 9 / 10 - 40, 20, 75, 75);
            }
            else
            {
                using (FileStream fs = new FileStream(assemblyFolder + "\\Resources\\logo.png", FileMode.Open, FileAccess.Read))
                {
                    image = Image.FromStream(fs);
                }
                inputGraphics.DrawImage(image, (float)pageIn.Width * 9 / 10 - 40, 20, 75, 75);
            }
            inputGraphics.DrawImage(Properties.Resources.Motorbike_Blue, new Rectangle((int)((float)pageIn.Width * 9 / 10 - 120), 195, 40, 20));
            inputGraphics.DrawImage(Properties.Resources.Truck_Red, new Rectangle((int)((float)pageIn.Width * 9 / 10 - 77), 195, 40, 20));
            inputGraphics.DrawImage(Properties.Resources.Car_Yellow, new Rectangle((int)((float)pageIn.Width * 9 / 10 - 34), 195, 40, 20));
            inputGraphics.DrawImage(Properties.Resources.Bus_Green, new Rectangle((int)((float)pageIn.Width * 9 / 10 + 9), 195, 40, 20));
            /*
            using (FileStream fs = new FileStream(assemblyFolder + "\\Resources\\ccar.png", FileMode.Open, FileAccess.Read))
            {
                image = Image.FromStream(fs);
            }
            inputGraphics.DrawImage(image, 10, 6, 292, 65);*/

            outputDocument.Info.Title = "Car Count Report";

            XFont fontSmaller = new XFont(
                "Verdana", 9, XFontStyle.Regular);

            double fontHeight = fontSmaller.GetHeight();
            XFont captionCameraSelectedFont = new XFont(
                "Verdana", 14, XFontStyle.BoldItalic);
            int i = 0;

            string Address = Vehicle_Counter_V2Definition.getXmlOption(Vehicle_Counter_V2Definition.OPTION_REPORT_ADDRESS, tempConfig);
            if (String.IsNullOrWhiteSpace(Address))
            {
                Address = "19 Dock Road\r\nV&A Waterfront Cape Town";
            }

            string[] addressSplit = Address.Replace("\r", "\n").Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string adrLine in addressSplit)
            {
                inputGraphics.DrawString(adrLine, fontSmaller, XBrushes.Black,
                new XRect(pageIn.Width * 65 / 80, fontCaptionHeight * 7 + (fontHeight * i), pageIn.Width / 10, fontHeight),
                XStringFormats.TopLeft);
                i++;
            }

            if (!String.IsNullOrWhiteSpace(listGroup.Text))
            {
                inputGraphics.DrawString("Group Selected:", captionCameraSelectedFont, XBrushes.Black,
                    new XRect(pageIn.Width * 4 / 80, fontCaptionHeight * 7, pageIn.Width / 30, fontCaptionHeight),
                    XStringFormats.TopLeft);

                inputGraphics.DrawString(listGroup.Text, fontSmaller, XBrushes.Black,
                    new XRect(pageIn.Width * 4 / 80, fontCaptionHeight * 7 + (fontHeight * i), pageIn.Width / 30, fontHeight),
                    XStringFormats.TopLeft);
            }

            /*foreach (CheckedData item in CameraList.Where(x => x.Checked).OrderBy(x => x.Name))
            {
                int cameraWhereInbound = 0;
                int cameraWhereOutbound = 0;
                using (var connection = GetSqlConnection())
                {
                    using (var context = new VehicleDetectContext(connection, contextOwnsConnection: false))
                    {
                        cameraWhereInbound = context.VehicleCounts.Where(vc => vc.CountedAt >= universalTimeStart && vc.CountedAt < universalTimeEnd).Where(x => x.CameraItemID == item.GUID && x.Inbound).Count();
                        cameraWhereOutbound = context.VehicleCounts.Where(vc => vc.CountedAt >= universalTimeStart && vc.CountedAt < universalTimeEnd).Where(x => x.CameraItemID == item.GUID && !x.Inbound).Count();
                    }
                }

                inputGraphics.DrawString(item.Name + " (" + cameraWhereInbound + " in, " + cameraWhereOutbound + " out)", fontSmaller, XBrushes.Black,
                new XRect(pageIn.Width * 4 / 80, fontCaptionHeight * 7 + (fontHeight * i), pageIn.Width / 30, fontHeight),
                XStringFormats.TopLeft);
                i++;
            }*/

            /********************************************************************************
             * Add the table with the individual counts
             ********************************************************************************/
            MigraDoc.DocumentObjectModel.Document doc = new MigraDoc.DocumentObjectModel.Document();
            doc.DefaultPageSetup.PageFormat = MigraDoc.DocumentObjectModel.PageFormat.A4;
            doc.DefaultPageSetup.Orientation = MigraDoc.DocumentObjectModel.Orientation.Landscape;
            var section = doc.AddSection();
            Table table = section.AddTable();
            var dateColumn = table.Columns.AddColumn();
            dateColumn.Width = "6.5cm";
            dateColumn.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Left;
            table.Columns.AddColumn().Width = "0.3cm";
            var bikeIn = table.Columns.AddColumn();
            bikeIn.Width = "1.8cm";
            bikeIn.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            var bikeOut = table.Columns.AddColumn();
            bikeOut.Width = "1.8cm";
            bikeOut.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            table.Columns.AddColumn().Width = "0.3cm";
            var truckIn = table.Columns.AddColumn();
            truckIn.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            truckIn.Width = "1.8cm";
            var truckOut = table.Columns.AddColumn();
            truckOut.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            truckOut.Width = "1.8cm";
            table.Columns.AddColumn().Width = "0.3cm";
            var carIn = table.Columns.AddColumn();
            carIn.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            carIn.Width = "1.8cm";
            var carOut = table.Columns.AddColumn();
            carOut.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            carOut.Width = "1.8cm";
            table.Columns.AddColumn().Width = "0.3cm";
            var busIn = table.Columns.AddColumn();
            busIn.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            busIn.Width = "1.8cm";
            var busOut = table.Columns.AddColumn();
            busOut.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            busOut.Width = "1.8cm";
            table.Columns.AddColumn().Width = "0.3cm";
            var totalIn = table.Columns.AddColumn();
            totalIn.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            totalIn.Width = "1.8cm";
            var totalOut = table.Columns.AddColumn();
            totalOut.Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            totalOut.Width = "1.8cm";

            var row = table.AddRow();
            row.Format.Font.Bold = true;
            var dates = row.Cells[0].AddParagraph(StartDate.ToShortDateString() + " to " + EndDate.ToShortDateString());
            row.Cells[0].Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[2].MergeRight = 1;
            if (motorbikeHidden)
            {
                var imageBike = row.Cells[2].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Motorbike-Blue-Disabled.png");
                imageBike.Width = "1.6cm";
            }
            else
            {
                var imageBike = row.Cells[2].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Motorbike-Blue.png");
                imageBike.Width = "1.6cm";
            } 
            row.Cells[2].Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[5].MergeRight = 1;
            if (truckHidden)
            {
                var imageTruck = row.Cells[5].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Truck-Red-Disabled.png");
                imageTruck.Width = "1.6cm";
            }
            else
            {
                var imageTruck = row.Cells[5].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Truck-Red.png");
                imageTruck.Width = "1.6cm";
            }            
            row.Cells[5].Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[8].MergeRight = 1;
            if (carHidden)
            {
                var imageCar = row.Cells[8].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Car-Yellow-Disabled.png");
                imageCar.Width = "1.6cm";
            }
            else
            {
                var imageCar = row.Cells[8].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Car-Yellow.png");
                imageCar.Width = "1.6cm";
            }            
            row.Cells[8].Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            row.Cells[8].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[11].MergeRight = 1;
            if (busHidden)
            {
                var imageBus = row.Cells[11].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Bus-Green-Disabled.png");
                imageBus.Width = "1.6cm";
            }
            else
            {
                var imageBus = row.Cells[11].AddParagraph().AddImage(assemblyFolder + "\\Resources\\Bus-Green.png");
                imageBus.Width = "1.6cm";
            }
            row.Cells[11].Format.Alignment = MigraDoc.DocumentObjectModel.ParagraphAlignment.Center;
            row.Cells[11].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row = table.AddRow();
            row = table.AddRow();
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Camera Name");
            row.Cells[2].AddParagraph("In");
            row.Cells[3].AddParagraph("Out");
            row.Cells[5].AddParagraph("In");
            row.Cells[6].AddParagraph("Out");
            row.Cells[8].AddParagraph("In");
            row.Cells[9].AddParagraph("Out");
            row.Cells[11].AddParagraph("In");
            row.Cells[12].AddParagraph("Out");
            row.Cells[14].AddParagraph("Total In");
            row.Cells[15].AddParagraph("Total Out");

            var valueDictionary = GetPerCameraCounts();
            foreach (var value in valueDictionary)
            {
                row = table.AddRow();
                row.Cells[0].AddParagraph(value.Key);
                row.Cells[2].AddParagraph(value.Value["Motorbike"].Key.ToString());
                row.Cells[3].AddParagraph(value.Value["Motorbike"].Value.ToString());
                row.Cells[5].AddParagraph(value.Value["Truck"].Key.ToString());
                row.Cells[6].AddParagraph(value.Value["Truck"].Value.ToString());
                row.Cells[8].AddParagraph(value.Value["Car"].Key.ToString());
                row.Cells[9].AddParagraph(value.Value["Car"].Value.ToString());
                row.Cells[11].AddParagraph(value.Value["Bus"].Key.ToString());
                row.Cells[12].AddParagraph(value.Value["Bus"].Value.ToString());
                row.Cells[14].AddParagraph(value.Value.Sum(x => x.Value.Key).ToString());
                row.Cells[15].AddParagraph(value.Value.Sum(x => x.Value.Value).ToString());
            }

            row = table.AddRow();
            row = table.AddRow();
            row.Cells[0].AddParagraph("Totals");
            row.Cells[2].AddParagraph(valueDictionary.Sum(y => y.Value["Motorbike"].Key).ToString());
            row.Cells[3].AddParagraph(valueDictionary.Sum(y => y.Value["Motorbike"].Value).ToString());
            row.Cells[5].AddParagraph(valueDictionary.Sum(y => y.Value["Truck"].Key).ToString());
            row.Cells[6].AddParagraph(valueDictionary.Sum(y => y.Value["Truck"].Value).ToString());
            row.Cells[8].AddParagraph(valueDictionary.Sum(y => y.Value["Car"].Key).ToString());
            row.Cells[9].AddParagraph(valueDictionary.Sum(y => y.Value["Car"].Value).ToString());
            row.Cells[11].AddParagraph(valueDictionary.Sum(y => y.Value["Bus"].Key).ToString());
            row.Cells[12].AddParagraph(valueDictionary.Sum(y => y.Value["Bus"].Value).ToString());
            row.Cells[14].AddParagraph(valueDictionary.Sum(x => x.Value.Sum(y => y.Value.Key)).ToString());
            row.Cells[15].AddParagraph(valueDictionary.Sum(x => x.Value.Sum(y => y.Value.Value)).ToString());

            var renderer = new MigraDoc.Rendering.PdfDocumentRenderer();
            renderer.Document = doc;
            renderer.RenderDocument();
            MemoryStream pdfmemstream = new MemoryStream();
            renderer.PdfDocument.Save(pdfmemstream);
            pdfmemstream.Seek(0, SeekOrigin.Begin);
            outputDocument.Pages.Add(PdfReader.Open(pdfmemstream, PdfDocumentOpenMode.Import).Pages[0]);

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "PDF Files (*.pdf)|*.pdf";
            dialog.AddExtension = true;
            dialog.DefaultExt = ".pdf";
            string filename = ("Car_counts_" + StartDate.ToShortDateString() + "_to_" + EndDate.ToShortDateString() + ".pdf").Replace(' ', '_').Replace('/', '_');

            if (CameraList.Where(x => x.Checked).Count() == 1)
            {
                filename = (CameraList.Where(x => x.Checked).First().Name + "_" + StartDate.ToShortDateString() + "_to_" + EndDate.ToShortDateString() + ".pdf").Replace(' ', '_').Replace('/', '_');
            }
            dialog.FileName = filename;

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                outputDocument.Save(dialog.FileName);

                Process.Start(dialog.FileName);
            }

            outputDocument.Close();

            File.Delete(Path.GetTempPath() + guid.ToString() + "_out_pdfgraph.pdf");
            File.Delete(Path.GetTempPath() + guid.ToString() + "_in_pdfgraph.pdf");

            LinearCountAxisInbound.LabelFormatter = _formatter;
            LinearCountAxisOutbound.LabelFormatter = _formatter;
            LinearCountAxisInbound.Maximum /= (1.4 / 1.1);
            LinearCountAxisOutbound.Maximum /= (1.4 / 1.1);

            OutboundCarCountSeries.LabelFormatString = "";
            OutboundBusCountSeries.LabelFormatString = "";
            OutboundTruckCountSeries.LabelFormatString = "";
            OutboundMotorbikeCountSeries.LabelFormatString = "";
            InboundCarCountSeries.LabelFormatString = "";
            InboundBusCountSeries.LabelFormatString = "";
            InboundTruckCountSeries.LabelFormatString = "";
            InboundMotorbikeCountSeries.LabelFormatString = "";

            Dispatcher.InvokeAsync(() =>
            {
                barChartIncoming.ResetAllAxes();
                barChartOutgoing.ResetAllAxes();
                barChartIncoming.InvalidatePlot();
                barChartOutgoing.InvalidatePlot();
            });

        }

        private static string originalLabelFormat = "";

        private void DateBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (comboBoxPeriodSelection.SelectedIndex != 5)
            {
                return;
            }
            buttonGenerate_Click(sender, e);
        }

        private void DateBox_MouseDown(object sender, EventArgs e)
        {
            comboBoxPeriodSelection.SelectedIndex = 5;
        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int selectedPeriod = -1;
            for (int i = 0; i < periodStr.Length; i++)
            {
                if (comboBoxPeriodSelection.SelectedItem.ToString() == periodStr[i])
                {
                    selectedPeriod = i;
                    break;
                }
            }

            switch (selectedPeriod)
            {
                case 0: //Today
                    StartDatePicker.SelectedDate = DateTime.Today;
                    EndDatePicker.SelectedDate = DateTime.Today;
                    break;
                case 1: //Yesterday
                    StartDatePicker.SelectedDate = DateTime.Today.AddDays(-1);
                    EndDatePicker.SelectedDate = DateTime.Today.AddDays(-1);
                    break;
                case 2: //Last 7 days
                    StartDatePicker.SelectedDate = DateTime.Today.AddDays(-7);
                    EndDatePicker.SelectedDate = DateTime.Today;
                    break;
                case 3://Last week
                    DateTime lastWeek = DateTime.Today.AddDays(-7);
                    DateTime startWeek = lastWeek.AddDays(-((int)(lastWeek.DayOfWeek + 1) % 7));
                    DateTime endWeek = startWeek.AddDays(7).AddMilliseconds(-1);
                    StartDatePicker.SelectedDate = startWeek;
                    EndDatePicker.SelectedDate = endWeek;
                    break;
                case 4: //Last 30 days
                    StartDatePicker.SelectedDate = DateTime.Today.AddDays(-30);
                    EndDatePicker.SelectedDate = DateTime.Today;
                    break;
                case 5://Last month
                    DateTime lastMonth = DateTime.Today.AddMonths(-1);
                    DateTime startMonth = new DateTime(lastMonth.Year, lastMonth.Month, 1);
                    DateTime endMonth = startMonth.AddMonths(1).AddMilliseconds(-1);
                    StartDatePicker.SelectedDate = startMonth;
                    EndDatePicker.SelectedDate = endMonth;
                    break;
                case 6: //Last year
                    StartDatePicker.SelectedDate = DateTime.Today.AddYears(-1);
                    EndDatePicker.SelectedDate = DateTime.Today;
                    break;
                case 7: //else custom
                    break;
            }
            UpdateReportClick(sender, e);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            GroupList.ForEach(y => CameraList.Where(x => x.Group == y.Name).ToList().ForEach(x => x.Checked = y.Checked));
            listCameras.ItemsSource = null;
            listCameras.ItemsSource = CameraList;
        }

        private void CheckBox_Click_Camera(object sender, RoutedEventArgs e)
        {
            GroupList.ForEach(x => x.Checked = CameraList.Where(y => y.Group == x.Name).All(y => y.Checked));
            listGroup.SelectedIndex = -1;
            buttonGenerate_Click(sender, e);
        }

        TcpClient tcp = null;

        private void cameraStreamThread(object obj)
        {
            while (running)
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    if (cameraSelectBox.SelectedIndex < 0 || (cameraSelectBox.SelectedItem as VehicleDetector) == null || (cameraSelectBox.SelectedItem as VehicleDetector).CameraItem == null)
                    {
                        return;
                    }
                    cameraFQID = (cameraSelectBox.SelectedItem as VehicleDetector).CameraItem.FQID.ObjectId.ToString();
                }));
                if (cameraFQID == null)
                {
                    Thread.Sleep(500);
                    continue;
                }
                
                if (tcp == null)
                {
                    try
                    {
                        tcp = new TcpClient("127.0.0.1", 8888);
                    }
                    catch (Exception)
                    {
                        tcp = null;
                    }
                }

                try
                {
                    byte[] buffer = UTF8Encoding.UTF8.GetBytes(cameraFQID);
                    tcp.ReceiveTimeout = 100;
                    NetworkStream stream = tcp.GetStream();
                    stream.Write(buffer, 0, buffer.Length);
                    byte[] receiveBuffer = new byte[4];
                    if (stream.Read(receiveBuffer, 0, 4) == 4)
                    {
                        int length = (int)BitConverter.ToUInt32(receiveBuffer, 0);
                        if (length > 0)
                        {

                            Dispatcher.Invoke(new Action(() =>
                            {
                                buffer = new byte[length];
                                int size = tcp.GetStream().Read(buffer, 0, length);
                                int packetcount = 0;
                                while (size < length && packetcount++ < 2 && size > 0)
                                {
                                    size = size + tcp.GetStream().Read(buffer, size, length - size);
                                }
                                if (size == length)
                                {
                                    BitmapImage bitmap = new BitmapImage();
                                    bitmap.BeginInit();
                                    bitmap.StreamSource = new MemoryStream(buffer);
                                    bitmap.CacheOption = BitmapCacheOption.None;
                                    bitmap.EndInit();
                                    VideoImage.Source = bitmap;
                                }
                            }));
                        }
                    }
                    tcp.Close();
                    tcp = null;
                }
                catch (Exception)
                {
                    try
                    {
                        tcp.Close();
                    }
                    catch (Exception)
                    {

                    }
                    tcp = null;
                }
            }
        }

        volatile string cameraFQID = null;

        private void cameraSelectBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            
            if (vc != null)
            {
                vc.Disconnect();
                vc.Close();
                vc.Dispose();
                VideoGrid.Children.Clear();
            }

            vc = new ImageViewerWpfControl();
            VideoGrid.Children.Add(vc);

            vc.Name = "ViewerControl";
            vc.CameraFQID = (cameraSelectBox.SelectedItem as VehicleDetector).CameraItem.FQID;
            vc.Initialize();
            vc.Connect();
            cameraFQID = (cameraSelectBox.SelectedItem as VehicleDetector).CameraItem.FQID.ObjectId.ToString();
        }

        private volatile string finalLabelText;
        private volatile static object usingConnection = new object();
        private volatile static bool inuse = false;

        private void timerUpdateCounts_Tick(object sender, EventArgs e)
        {

            var connection = SqlUtility.GetSqlConnection();
            if (connection == null)
            {
                return;
            }

            finalLabelText = "Please select camera to view";

            lock (usingConnection)
            {
                if (inuse)
                {
                    return;
                }
                inuse = true;
            }
            using (connection)
            {
                using (var context = new VehicleDetectContext(connection, contextOwnsConnection: false))
                {
                    DateTime startOfHour = DateTime.Now.ToUniversalTime();
                    startOfHour = startOfHour.AddMilliseconds(-startOfHour.Millisecond);
                    startOfHour = startOfHour.AddSeconds(-startOfHour.Second);
                    startOfHour = startOfHour.AddMinutes(-startOfHour.Minute);
                   
                    var inVehicleCountHour  = context.VehicleCounts.Where(x => x.CountedAt > startOfHour && x.Inbound  && x.CameraItemID == cameraFQID).ToList();
                    var outVehicleCountHour = context.VehicleCounts.Where(x => x.CountedAt > startOfHour && !x.Inbound && x.CameraItemID == cameraFQID).ToList();

                    Dispatcher.Invoke(() =>
                    {
                        labelInboundMotorbike.Content = inVehicleCountHour.Where(x => x.ObjectName == "motorbike").Count();
                        labelInboundCar.Content = inVehicleCountHour.Where(x => x.ObjectName == "car" || x.ObjectName == null).Count();
                        labelInboundTruck.Content = inVehicleCountHour.Where(x => x.ObjectName == "truck").Count();
                        labelInboundBus.Content = inVehicleCountHour.Where(x => x.ObjectName == "bus").Count();
                        labelOutboundMotorbike.Content = outVehicleCountHour.Where(x => x.ObjectName == "motorbike").Count();
                        labelOutboundCar.Content = outVehicleCountHour.Where(x => x.ObjectName == "car" || x.ObjectName == null).Count();
                        labelOutboundTruck.Content = outVehicleCountHour.Where(x => x.ObjectName == "truck").Count();
                        labelOutboundBus.Content = outVehicleCountHour.Where(x => x.ObjectName == "bus").Count();
                        labelCountHour.Content = "Vehicles since " + startOfHour.ToLocalTime().Hour + ":00";
                    });
                    
                }
            }
            lock (usingConnection)
            {
                inuse = false;
            }
        }

        private void listGroup_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (listGroup.SelectedIndex < 0)
            {
                return;
            }
            CameraList.ForEach(x => x.Checked = false);
            GroupList.ForEach(x => x.Checked = false);
            (listGroup.SelectedItem as CheckedData).Checked = true;
            GroupList.ForEach(y => CameraList.Where(x => x.Group == (listGroup.SelectedItem as CheckedData).Name).ToList().ForEach(x => x.Checked = true));
            listCameras.ItemsSource = null;
            listCameras.ItemsSource = CameraList.OrderBy(x => x.Name);
            buttonGenerate_Click(sender, e);
        }

        private void listCameras_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }

        private void BusClick(object sender, RoutedEventArgs e)
        {
            if (busHidden)
            {
                busHidden = false;
                busImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Bus-Green.png", UriKind.Relative));
            }
            else
            {
                busHidden = true;
                busImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Bus-Green-Disabled.png", UriKind.Relative));
            }
            UpdateReportClick(sender, e);
        }

        private void MotorbikeClick(object sender, RoutedEventArgs e)
        {
            if (motorbikeHidden)
            {
                motorbikeHidden = false;
                motorbikeImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Motorbike-Blue.png", UriKind.Relative));
            }
            else
            {
                motorbikeHidden = true;
                motorbikeImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Motorbike-Blue-Disabled.png", UriKind.Relative));               
            }
            UpdateReportClick(sender, e);
        }

        private void CarClick(object sender, RoutedEventArgs e)
        {
            if (carHidden)
            {
                carHidden = false;
                carImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Car-Yellow.png", UriKind.Relative));
            }
            else
            {
                carHidden = true;
                carImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Car-Yellow-Disabled.png", UriKind.Relative));
            }
            UpdateReportClick(sender, e);
        }

        private void TruckClick(object sender, RoutedEventArgs e)
        {
            if (truckHidden)
            {
                truckHidden = false;
                truckImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Truck-Red.png", UriKind.Relative));
                           
            }
            else
            {
                truckHidden = true;
                truckImage.Source = new BitmapImage(new Uri(@"/VehicleCounterV2;component/Resources/Truck-Red-Disabled.png", UriKind.Relative));
            }
            UpdateReportClick(sender, e);
        }
    }
}
