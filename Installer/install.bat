@echo off
start /wait msiexec /i "Radinium Classifier.msi"
IF %ERRORLEVEL% NEQ 0 ( 
   echo "Installation cancelled"
   EXIT /B
)
IF EXIST \\*filepath or .exe*\\ GOTO :eof

echo Please wait for the installation to complete...
timeout /T 5 /nobreak
echo Installing OpenJDK
start /wait "" msiexec /quiet /qn /norestart /i java-1.8.0-openjdk-1.8.0.181-1.b13.ojdkbuild.windows.x86_64.msi
echo Installing Classifier Phase 1
start /wait "" apache-tomcat-7.0.90.exe /S /C=tomcat.ini /D=C:\Program Files\Apache Tomcat
timeout /T 8 /nobreak
echo Continuing installation of Classifier
"C:\Program Files\Apache Tomcat\bin\Tomcat7.exe" //US//Tomcat7 --StdOutput NUL
timeout /T 6 /nobreak
echo Setting Classifier to auto-start
sc config Tomcat7 start=delayed-auto

copy activiti-app.war "c:\program files\apache tomcat\webapps"
copy sqljdbc_auth.dll "c:\program files\apache tomcat\bin"
copy sqljdbc42.jar "c:\program files\apache tomcat\lib"
copy /Y logging.properties "c:\program files\apache tomcat\conf"
echo Initializing workflow

NET start Tomcat7
echo Stopping Milestone Event Server for final steps

NET stop MilestoneEventServerService
setlocal enabledelayedexpansion
set "string=abcdefghijklmnopqrstuvwxyz"
set "result="
for /L %%i in (1,1,30) do call :add
set RANDOMTEXT=%result%AB_2

echo Updating workflow

sc stop Tomcat7 > NUL
echo Stopping workflow process after initializing
:stoptomcatloop
sc query Tomcat7 | find "STOPPED" > NUL
if errorlevel 1 (
  timeout 1 > NUL
  echo|set /p="."
  goto stoptomcatloop
)
echo Classifier process stopped
echo admin.password=%RANDOMTEXT%>> activiti-app.properties
echo datasource.password=%RANDOMTEXT%>> activiti-app.properties
copy /Y activiti-app.properties "C:\Program Files\Apache Tomcat\webapps\activiti-app\WEB-INF\classes\META-INF\activiti-app"
echo ALTER LOGIN activiti WITH PASSWORD = '%RANDOMTEXT%' UNLOCK; > UpdateActivitiPassword.sql
echo GO >> UpdateActivitiPassword.sql
echo UPDATE [activiti].[activiti].[ACT_ID_USER] SET PWD_='%RANDOMTEXT%' WHERE ID_='admin'; >> UpdateActivitiPassword.sql
echo GO >> UpdateActivitiPassword.
FOR /F "tokens=* USEBACKQ" %%F IN (`dir /b /s "c:\program files\*sqlcmd.exe"`) DO (
SET SQLCMD="%%F"
goto endsqlcmd
)
:endsqlcmd
%SQLCMD% -S 127.0.0.1 -i UpdateActivitiPassword.sql

echo Cleaning up stale files

del UpdateActivitiPassword.sql
del logging.properties
del sqljdbc_auth.dll
del sqljdbc42.jar
del activiti-app.war

echo Stopping SQL Server for installation
NET stop MSSQLSERVER
timeout /T 10 /nobreak

echo Starting SQL Server
NET start MSSQLSERVER
timeout /T 10 /nobreak

echo Starting workflow
NET start Tomcat7
timeout /T 30 /nobreak
echo Starting Milestone Event server
NET start MilestoneEventServerService

goto :eof

:add
set /a x=%random% %% 26 
set result=%result%!string:~%x%,1!
goto :eof